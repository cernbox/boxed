#!/bin/sh
#set -o errexit # bail out on all errors immediately

# WARNING: THIS IS USED TO ADD SSO USERS TO OUR LOCAL LDAP SERVER

# We expect UID provided by the SSO to be numerical values that can be used 
# directly as uidNumber and gidNumber at the Unix level


# Delete temporary ldif files
delete_ldif_file () {
  rm -f $NEW_ENTRY_FNAME
}


###
### 0. CLI parser
for i in "$@"
do
case $i in
    -s=*|--ssouid=*)
    SSOUID="${i#*=}"
    shift
    ;;
    -d=*|--displayname=*)
    DISPLAYNAME="${i#*=}"
    shift
    ;;
    -n=*|--name=*)
    GIVENNAME="${i#*=}"
    shift
    ;;
    -f=*|--surname=*)
    SURNAME="${i#*=}"
    shift
    ;;
    -m=*|--mail=*)
    MAIL="${i#*=}"
    shift
    ;;

    -*)
    echo "Unknown option: $i"
    exit -1
    ;;
esac
done

# Check the mandatory variables are set
if [ "$SSOUID" == "--" ] || [ "$GIVENNAME" == "--" ] || [ "$SURNAME" == "--" ]|| [ "$MAIL" == "--" ]; then
  echo "ERROR: One or more required variabls not set"
  exit -1
fi


###
### 1. Check if the entry in LDAP already exists
ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b uid=$SSOUID,$LDAP_BASE_DN "(objectClass=ssoUnixMatch)"
if [ $? -eq 0 ]; then
  echo "INFO: SSO uid already known. No further actions required"
  exit 1
fi

# Create LDIF file for the new entry
NEW_ENTRY_FNAME="/tmp/entry_$SSOUID.ldif"
echo "dn: uid=$SSOUID,$LDAP_BASE_DN
objectclass: top
objectclass: account
objectclass: ssoUnixMatch
ssouid: $SSOUID
displayname: $DISPLAYNAME
givenname: $GIVENNAME
sn: $SURNAME
mail: $MAIL
uid: $SSOUID
uidNumber: $SSOUID
gidNumber: $SSOUID
cn: $DISPLAYNAME
homeDirectory: /home/$SSOUID
loginShell: /bin/bash
gecos: $SSOUID" > $NEW_ENTRY_FNAME

# Add the entry to LDAP. Clean-up and bail out if anything fails
echo "Adding new entry to LDAP..."
ldapadd -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $NEW_ENTRY_FNAME
if [ $? -ne 0 ]; then
  echo "ERROR: LDAP failed on adding entry."
  delete_ldif_file
  exit -1
fi

echo "Entry added successfully."
delete_ldif_file

exit 0

