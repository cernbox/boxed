#!/bin/bash

# WARNING: THIS IS USED TO LOOKUP FOR USER NAME and SURNAME IN THE LDAP SERVER


### Decode user names in case those are stored base64-encoded in LDAP
try_decode_base64 () {
  decoded=`echo $1 | base64 --decode`
  if [ $? -eq 0 ]; then
     echo $decoded
     return 0
   fi
   echo $1
   return 1
}


### CLI parameters
if [ "$#" -ne 2 ]; then
  echo "Illegal number of parameters"
  echo "syntax: $0 <lookup_key> <lookup_value>"
  echo "example: $0 uid userid00"
  exit -1
fi
LOOKUP_KEY=$1
LOOKUP_VALUE=$2


### Check if the user exists in ldap
result=`ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b $LDAP_BASE_DN "(&($LOOKUP_KEY=$LOOKUP_VALUE)(objectClass=ssoUnixMatch))"`
entry_found=`echo "$result" | grep '^# numEntries:' | wc -l`
if [ $entry_found -eq 0 ]; then
  # No matching entries were found
  exit 1
else
  num_entries=`echo "$result" | grep '^# numEntries:' | tr -d ' ' | cut -d ':' -f 2`
  if [ $num_entries -gt 1 ]; then
    # There are too many matching entries
    exit 2
  fi
fi


### Retrieve (or build) a proper display name
dn=`echo "$result" | grep '^displayName:' | head -n 1 | cut -c 13-`
gn=`echo "$result" | grep '^givenName:' | head -n 1 | cut -c 11-`
sn=`echo "$result" | grep '^sn:' | head -n 1 | cut -c 4-`

# If a display name is given, just use it (decoding base64, if needed)
if [[ $dn != " --" ]]; then
  if [[ `echo $dn | cut -c 1,2` == ": " ]]; then
    output="$( try_decode_base64 `echo $dn | cut -c 3-` )"
  else
    output=$dn
  fi
  echo $output
  exit 0

# Otherwise, build a display name from given name and surname
elif [[ $gn != " --" ]] && [[ $sn != " --" ]]; then
  # Given name
  if [[ `echo $gn | cut -c 1,2` == ": " ]]; then
    output="$( try_decode_base64 `echo $gn | cut -c 3-` )"
  else
    output=$gn
  fi
  # Surname
  if [[ `echo $sn | cut -c 1,2` == ": " ]]; then
    output="$output $( try_decode_base64 `echo $sn | cut -c 3-` )"
  else
    output="$output $sn"
  fi
  # Return the string built from given name and surname
  echo $output
  exit 0

# If we have no metadata about user names, return dashes
else
  echo "--"
  exit 3
fi

