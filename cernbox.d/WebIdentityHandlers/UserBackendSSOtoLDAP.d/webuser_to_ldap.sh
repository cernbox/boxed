#!/bin/sh
#set -o errexit # bail out on all errors immediately

# WARNING: THIS IS USED TO ADD SSO USERS TO OUR LOCAL LDAP SERVER


# Delete temporary ldif files
delete_ldif_files () {
  rm -f $SSO_ENTRY_FNAME $UNIX_ENTRY_FNAME $MATCH_ENTRY_FNAME
}


### LDAP entries parameters
ENTRIES_COUNT=`ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b $LDAP_BASE_DN "(&(objectClass=unixAccount)(uidNumber=*))" -s one numsubordinates | grep "numEntries" | tr -d ' ' | cut -d ':' -f 2`


### 0. CLI parser
for i in "$@"
do
case $i in
    -s=*|--ssouid=*)
    SSOUID="${i#*=}"
    shift
    ;;
    -d=*|--displayname=*)
    DISPLAYNAME="${i#*=}"
    shift
    ;;
    -n=*|--name=*)
    GIVENNAME="${i#*=}"
    shift
    ;;
    -f=*|--surname=*)
    SURNAME="${i#*=}"
    shift
    ;;
    -m=*|--mail=*)
    MAIL="${i#*=}"
    shift
    ;;

    -*)
    echo "Unknown option: $i"
    exit -1
    ;;
esac
done

# Check the mandatory variables are set
if [ "$SSOUID" == "--" ] || [ "$GIVENNAME" == "--" ] || [ "$SURNAME" == "--" ]|| [ "$MAIL" == "--" ]; then
  echo "ERROR: One or more required variabls not set"
  exit -1
fi


###
### 1. Check if the entry in LDAP already exists
ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b ssouid=$SSOUID,$LDAP_BASE_DN "(objectClass=ssoUnixMatch)"
if [ $? -eq 0 ]; then
  echo "INFO: SSO uid already known. No further actions required"
  exit 1
fi


###
### 2. If the user is new to LDAP, create new entries for her
if [ -z $ENTRIES_COUNT ]; then 
  NEWENTRY_UIDNUMBER=$LDAP_ENTRY_UIDNUMBER_START
else
  NEWENTRY_UIDNUMBER=$((LDAP_ENTRY_UIDNUMBER_START+ENTRIES_COUNT))
fi
NEWENTRY_NAME=$LDAP_ENTRY_NAME_PREFIX`printf "%06d" $NEWENTRY_UIDNUMBER`


###
### 3. Make sure the uidNumber is not allocated yet
RETRY_COUNT=0
RETRY_MAX=1000

ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b uid=$NEWENTRY_NAME,$LDAP_BASE_DN "(objectClass=ssoUnixMatch)" > /dev/null
result=`echo $?`
while [ $result -ne 32 ] && [ $RETRY_COUNT -lt $RETRY_MAX ];
do
  NEWENTRY_UIDNUMBER=$((NEWENTRY_UIDNUMBER+1))
  NEWENTRY_NAME=$LDAP_ENTRY_NAME_PREFIX`printf "%06d" $NEWENTRY_UIDNUMBER`
  RETRY_COUNT=$((RETRY_COUNT+1))
  ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b uid=$NEWENTRY_NAME,$LDAP_BASE_DN "(objectClass=ssoUnixMatch)" > /dev/null
  result=`echo $?`
done

if [ $RETRY_COUNT -eq $RETRY_MAX ];
then
  echo "ERROR: Unable to find a free uidNumber for user $SSOUID after $RETRY_MAX attempts"
  exit -1
fi


###
### 4. Add the user to LDAP

# Create LDIF file for the SSO entry
SSO_ENTRY_FNAME="/tmp/SSO_$NEWENTRY_UIDNUMBER.ldif"
echo "dn: ssouid=$SSOUID,$LDAP_BASE_DN
objectclass: top
objectclass: account
objectclass: ssoAttributes
uid: $SSOUID
ssouid: $SSOUID
displayname: $DISPLAYNAME
givenname: $GIVENNAME
sn: $SURNAME
mail: $MAIL" > $SSO_ENTRY_FNAME

# Create LDIF file for the Unix Account entry
UNIX_ENTRY_FNAME="/tmp/Unix_$NEWENTRY_UIDNUMBER.ldif"
echo "dn: uid=$NEWENTRY_NAME,$LDAP_BASE_DN
objectclass: top
objectclass: account
objectclass: unixAccount
uid: $NEWENTRY_NAME
uidNumber: $NEWENTRY_UIDNUMBER
gidNumber: $NEWENTRY_UIDNUMBER
cn: $NEWENTRY_NAME
homeDirectory: /home/$NEWENTRY_NAME
loginShell: /bin/bash
gecos: $NEWENTRY_NAME" > $UNIX_ENTRY_FNAME

# Create LDIF file for Match entry between SSO and Unix
MATCH_ENTRY_FNAME="/tmp/Match_$NEWENTRY_UIDNUMBER.ldif"
echo "dn: uid=$NEWENTRY_NAME,ssouid=$SSOUID,$LDAP_BASE_DN
objectclass: top
objectclass: account
objectclass: ssoUnixMatch
ssouid: $SSOUID
displayname: $DISPLAYNAME
givenname: $GIVENNAME
sn: $SURNAME
mail: $MAIL
uid: $NEWENTRY_NAME
uidNumber: $NEWENTRY_UIDNUMBER
gidNumber: $NEWENTRY_UIDNUMBER
cn: $NEWENTRY_NAME" > $MATCH_ENTRY_FNAME

# Add the three entries to LDAP. Clean-up and bail out if anything fails
echo "Adding SSO entry to LDAP..."
ldapadd -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $SSO_ENTRY_FNAME
if [ $? -ne 0 ]; then
  echo "ERROR: LDAP failed on adding SSO entry."
  delete_ldif_files
  exit -1
fi

echo "Adding Unix entry to LDAP..."
ldapadd -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $UNIX_ENTRY_FNAME
if [ $? -ne 0 ]; then
  echo "ERROR: LDAP failed on adding Unix entry."
  echo "Cleaning up previously-added SSO entry..."
  ldapdelete -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $SSO_ENTRY_FNAME
  delete_ldif_files
  exit -1
fi

echo "Adding SSO--Unix matching entry to LDAP..."
ldapadd -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $MATCH_ENTRY_FNAME
if [ $? -ne 0 ]; then
  echo "ERROR: LDAP failed on adding SSO--Unix matching entry."
  echo "Cleaning up previously-added SSO and Unix entries..."
  ldapdelete -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $SSO_ENTRY_FNAME
  ldapdelete -x -H $LDAP_URI -D $LDAP_ADMIN_BIND_DN -w $LDAP_ADMIN_BIND_PASSWORD -f $UNIX_ENTRY_FNAME
  delete_ldif_files
  exit -1
fi

echo "Entries added successfully."
delete_ldif_files

exit 0

