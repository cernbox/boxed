### DOCKER FILE FOR eos-storage IMAGE BASED ON EOS CITRINE -- EOS 4.x Version ###

### PLEASE SPECIFY EOS AND XROOTD VERSIONS ###
# E.g., 
# export XRD_VERSION="4.8.4"
# export EOS_VERSION="4.4.23"
# export RELEASE_VERSION=":v0"
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/eos-storage${RELEASE_VERSION} -f eos-storage.citrine.Dockerfile --build-arg EOS_VERSION="-${EOS_VERSION}" --build-arg XRD_VERSION="-${XRD_VERSION}" .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/eos-storage${RELEASE_VERSION}
###


FROM cern/cc7-base:20180516

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8


# ----- Remove yum autoupdate ----- #
RUN yum -y remove \
    yum-autoupdate


# ----- Install tools for LDAP access ----- #
# Note: This will be used by the MGM only
RUN yum -y install \
    nscd \
    nss-pam-ldapd \
    openldap-clients
ADD ./ldappam.d /etc
RUN chmod 600 /etc/nslcd.conf
ADD ./ldappam.d/nslcd_foreground.sh /usr/sbin/nslcd_foreground.sh
RUN chmod +x /usr/sbin/nslcd_foreground.sh


# ----- Copy the repos to install eos and xrootd ----- #
ADD ./eos-storage.citrine.d/repos/*.repo /etc/yum.repos.d/


# ----- Install debugging tools ----- #
RUN yum -y install \
    gnutls \
    strace \
    gdb \
    leveldb \
    zeromq \
    net-tools \
    bind-utils


# ----- Install XRootD ----- #
ARG XRD_VERSION
RUN yum -y --disablerepo=cern install \
    xrootd$XRD_VERSION \
    xrootd-client$XRD_VERSION \
    xrootd-client-devel$XRD_VERSION \
    xrootd-client-libs$XRD_VERSION \
    xrootd-devel$XRD_VERSION \
    xrootd-private-devel$XRD_VERSION \
    xrootd-libs$XRD_VERSION \
    xrootd-server$XRD_VERSION \
    xrootd-server-devel$XRD_VERSION \
    xrootd-server-libs$XRD_VERSION \
    xrootd-selinux$XRD_VERSION


# ----- Install EOS ----- #
ARG EOS_VERSION
RUN yum -y --disablerepo=base --disablerepo=updates --disablerepo=cern install \
    libmicrohttpd \
    libmicrohttpd-devel \
    protobuf3 \
    jemalloc \
    jemalloc-devel
RUN yum -y --disablerepo=cern --disablerepo=xrootd-stable install \
    eos-server$EOS_VERSION \
    eos-client$EOS_VERSION
#    quarkdb
ADD ./eos-storage.citrine.d/config_eos/eos.sysconfig /etc/sysconfig/eos
ADD ./eos-storage.citrine.d/config_eos/fstfmd.dict /var/eos/md/


# ----- Copy the keytabs and set the correct permissions ----- #
ADD ./eos-storage.citrine.d/keytabs/* /etc/
RUN chown daemon:daemon /etc/eos*.keytab
RUN chmod 600 /etc/eos*.keytab


# ----- Make a copy of /var/eos and /var/log/eos in case this will be stored on a hostPath volume ----- #
RUN mkdir -p /tmp/var-eos /tmp/var-log-eos
RUN cp -r -p /var/eos/* /tmp/var-eos
RUN cp -r -p /var/log/eos/* /tmp/var-log-eos


# ----- Copy the configuration files for EOS components ----- #
# Note: Configuration files have to be modified in network/host/domain names so to be 
#       consistent with the configuration of other containers (e.g., SWAN, CERNBox)
ADD ./eos-storage.citrine.d/config_xroot/ /etc/
ADD ./eos-storage.citrine.d/start_xrootd/ /root/start_xrootd/
ADD ./eos-storage.citrine.d/config_eos/eosadmin /usr/sbin/eosadmin
RUN chmod +x /usr/sbin/eosadmin
ADD ./eos-storage.citrine.d/config_eos/eosfstregister /usr/sbin/eosfstregister
RUN chmod +x /usr/sbin/eosfstregister
ADD ./eos-storage.citrine.d/config_instance/ /root/config_instance/
ADD ./eos-storage.citrine.d/utils/remove_locks.sh /root/remove_locks.sh


# ----- Install supervisord and base configuration file ----- #
RUN yum -y install supervisor
ADD ./supervisord.d/supervisord.conf /etc/supervisord.conf
ADD ./supervisord.d/kill_supervisor.ini /etc/supervisord.d
ADD ./supervisord.d/kill_supervisor.py /etc/supervisord.d

# Copy supervisor ini files
ADD ./eos-storage.citrine.d/supervisord.d/xrootd_mgm.ini /etc/supervisord.d/xrootd_mgm.noload
ADD ./eos-storage.citrine.d/supervisord.d/xrootd_mq.ini /etc/supervisord.d/xrootd_mq.noload
ADD ./eos-storage.citrine.d/supervisord.d/xrootd_fst.ini /etc/supervisord.d/xrootd_fst.noload
ADD ./eos-storage.citrine.d/supervisord.d/bootstrap_instance_config.ini /etc/supervisord.d/bootstrap_instance_config.noload
ADD ./eos-storage.citrine.d/supervisord.d/register_fs.ini /etc/supervisord.d/register_fs.noload
ADD ./eos-storage.citrine.d/supervisord.d/remove_locks.ini /etc/supervisord.d/remove_locks.noload
ADD ./supervisord.d/nscd.ini /etc/supervisord.d/nscd.noload
ADD ./supervisord.d/nslcd.ini /etc/supervisord.d/nslcd.noload


# ----- Run crond under supervisor and copy configuration files for log rotation ----- #
ADD ./supervisord.d/crond.ini /etc/supervisord.d/crond.noload
ADD ./logrotate.d/logrotate /etc/cron.hourly/logrotate
RUN chmod +x /etc/cron.hourly/logrotate

# Remove unwanted hourly tasks
RUN rm -f \
    /etc/cron.d/eos-logs \
    /etc/cron.d/eos-reports


# ----- Install logrotate and copy configuration files to rotate EOS logs ----- #
RUN yum -y install logrotate
RUN mv /etc/logrotate.conf /etc/logrotate.defaults
ADD ./logrotate.d/logrotate.conf /etc/logrotate.conf

# Copy logrotate jobs for EOS
RUN rm -f /etc/logrotate.d/eos-logs
ADD ./logrotate.d/logrotate.jobs.d/eos-all /etc/logrotate.d/eos-all
ADD ./logrotate.d/logrotate.jobs.d/eos-mgm /etc/logrotate.d/eos-mgm


# ----- Run the setup script in the container ----- #
ADD ./eos-storage.citrine.d/start.sh /root/start.sh
CMD ["/bin/bash", "/root/start.sh"]

