Box-in-the-box: technical notes, tips and tricks
================================================



How-to admin guide
==================

===== How to completely wipe the host and restart from scratch =====
WARNING: This will cause the users' data to be permanently deleted
===> Stop and remove ALL containers
		docker stop `docker ps -a | tail -n+2 | awk '{print $NF}'`
		docker rm `docker ps -a | tail -n+2 | awk '{print $NF}'`
Note: containers can be abruptly removed using the -f, --force option
		docker rm -f `docker ps -a | tail -n+2 | awk '{print $NF}'`


===> Delete Docker Volumes
Note: Volumes cannot be deleted if the container using them is still running
--> to wipe LDAP configuration and user list
		docker volume rm ldap_config ldap_database

--> to wipe EOS data and users' files. 
WARNING: THIS WILL CAUSE DATA LOSS!
		docker volume rm eos-mgm eos-mq eos-fst{1..6}

--> to remove both LDAP and EOS volumes
		docker volume rm ldap_config ldap_database eos-mgm eos-mq eos-fst{1..6}

--> to remove ALL volumes
		docker volume rm `docker volume ls | tail -n+2 | awk '{print $NF}'`

Note:	Both LDAP and EOS volumes are declared as external in docker-compose.yaml. If missing, they are not automatically created by compose.
		Volumes are initialized by SetupHost.sh or can be created manually with the following command:
		for i in ldap_config ldap_database eos-mq eos-mgm eos-fst{1..6}; do docker volume create --name=$i; done

===> Delete ALL Docker Images
Note: This will cause docker/docker-compose to rebuild/download images at the next run
		docker rmi `docker images | tail -n+2 | awk '{print $3}'`



===== How to restart EOS leaving the other containers untouched =====
EOS core containers (i.e., eos-mgm, eos-mq, eos-fst*) are not managed by docker-compose but are instantiated and configured by eos-controller. 
EOS core containers are based on the eos-storage image, which is not build by docker-compose but by the SetupHost.sh script. 
This is due to the fact that docker-compose provides only basic scheduling functionalities (i.e., depends_on) that are not sufficient for the deployment of EOS.

To restart EOS it is needed to:
1.	Stop and remove the running containers
		docker stop eos-mgm eos-mq eos-fst{1..6}
		docker rm eos-mgm eos-mq eos-fst{1..6}
1b.	Delete EOS volumes*. 
	WARNING: THIS WILL CAUSE DATA LOSS!
2. Rebuild eos-storage image
		docker build -t eos-storage -f eos-storage.Dockerfile .
   Note: this is mandatory in case of changes to the EOS configuration files, i.e., files stored in eos-storage.d/ and subdirectories, or of modifications to eos-storage.Dockerfile
3.	Rebuild eos-controller images
		docker-compose build eos-controller
   Note: this is mandatory in case of changes to the deployment procedure of EOS, i.e., files stored in eos-controller.d/ and subdirectories, or of modifications to eos-controller.Dockerfile
4.	Start eos-controller, which will deploy EOS core containers
		docker-compose up -d eos-controller

*By deleting EOS volumes, all users' data will be permanently deleted.
		docker volume rm eos-mgm eos-mq eos-fst{1..6}
 If volumes are deleted, it is needed to recreate them manually:
		for i in ldap_config ldap_database eos-mq eos-mgm eos-fst{1..6}; do docker volume create --name=$i; done

Make sure to pre-create home directories:

                docker exec -it cernbox bash /createUsersHome.sh


===== How to restart EOS fuse container =====
WARNING: Single-user Jupyter servers eventually running might prevent this procedure to be successful.
		 It is recommended to stop and remove Single-user Jupyter servers before proceeding.
1.	Stop the running container
		docker stop eos-fuse
		docker rm eos-fuse

2.	Unmount and delete the eos/cvmfs folder on the host machine
		fusermount -u /tmp/SWAN-in-Docker/eos_mount 
		rmdir /tmp/SWAN-in-Docker/eos_mount

3.	Rebuild and restart the container via docker-compose
		docker-compose build eos-fuse
		docker-compose up -d eos-fuse



===== How to restart CVMFS container =====
WARNING: Single-user Jupyter servers eventually running might prevent this procedure to be successful.
		 It is recommended to stop and remove Single-user Jupyter servers before proceeding.
1.	Stop and remove the running container
		docker stop cvmfs
		docker rm cvmfs

2.	Unmount and delete the cvmfs folder on the host machine
		for i in `ls /tmp/SWAN-in-Docker/cvmfs_mount`; do fusermount -u /tmp/SWAN-in-Docker/cvmfs_mount/$i; done
		rm -rf /tmp/SWAN-in-Docker/cvmfs_mount/

3.	Rebuild and restart the container via docker-compose
		docker-compose build cvmfs
		docker-compose up -d cvmfs





===== Deployment issues with Ubuntu 14.04 LTS 64-bit =====
To deploy the services on a host running Ubuntu 14.04, it is needed to:

	1.	Grant extra security options to the containers exporting a fuse mount (i.e., cvmfs and eos-fuse)
		Security options must be defined in docker-compose.yaml, alongside existing cap_add/devices/... options:
		    security_opt:
		      - apparmor:unconfined

	2. Explicitly set the fuse-mounted folders as shared in the host OS
		The SetupHost.sh script must be modified by adding:
			for i in $CVMFS_FOLDER $EOS_FOLDER;
			do
				mkdir -p $i
				mount --bind $i $i
				mount --make-shared $i
			done
		This should be put after the clean-up section, more specifically after the line:
			touch /tmp/SWAN-in-Docker/DO_NOT_WRITE_ANY_FILE_HERE





===== List of components =====
List of docker images required for the deployment: 
cernbox
cernboxgateway
cernphsft/systemuser
eos-controller
eos-storage
selftest
ldap
osixia/ldap
smashbox
swan_cvmfs
swan_eos-fuse
swan_jupyterhub

List of base docker images used:
centos:6.9 (used by EOS, should be replaced by cern/slc6)
centos:7 (used by EOS, should be replaced by cern/cc7)
cern/slc6-base:20170406
cern/cc7-base:20170113
osixia/ldap:1.1.8

List of docker containers managed by docker-compose:
cernbox
cernboxgateway
cvmfs
eos-controller
eos-fuse
jupyterhub
ldap
ldap-ldapadd

List of other docker containers:
eos-fst1
eos-fst2
eos-fst3
eos-fst4
eos-fst5
eos-fst6
eos-mgm
eos-mq
selftest
smashbox
Note: These containers are either managed by SetupHost.sh or instantiated manually for testing purposes.

List of volumes:
eos-fst1
eos-fst2
eos-fst3
eos-fst4
eos-fst5
eos-fst6
eos-mgm
eos-mq
ldap_config
ldap_database

List of networks:
demonet (lcoal, bridged)


