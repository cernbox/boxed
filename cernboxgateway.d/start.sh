#!/bin/bash
#set -o errexit # Bail out on all errors immediately

echo "---${THIS_CONTAINER}---"

case $DEPLOYMENT_TYPE in
  "kubernetes")
    # Print PodInfo
    echo ""
    echo "%%%--- PodInfo ---%%%"
    echo "Pod namespace: ${PODINFO_NAMESPACE}"
    echo "Pod name: ${PODINFO_NAME}"
    echo "Pod IP: ${PODINFO_IP}"
    echo "Node name (of the host where the pod is running): ${PODINFO_NODE_NAME}" 
    echo "Node IP (of the host where the pod is running): ${PODINFO_NODE_IP}"

    echo "Deploying with configuration for Kubernetes..."

    # Self-register as gateway in EOS (if required)
    if [ `echo $EOS_GATEWAY_SELF_REGISTRATION | tr '[:upper:]' '[:lower:]'` = "true" ]; then
      echo "Registering gateway with $EOS_GATEWAY_AUTH authentication..."
      bash /root/configure_gateway.sh add $EOS_GATEWAY_AUTH
    fi

    # Create AUTH and blender subfolders (in case logs are stored on persistent volumes)
    mkdir -p /var/log/nginx/AUTH/
    mkdir -p /var/log/nginx/blender/

    # Start crond for logrotation in supervisor
    echo "Enabling crond for logrotation..."
    echo "" >> /etc/supervisord.conf
    cat /etc/supervisord.d/crond.noload >> /etc/supervisord.conf
    ;;

  "compose")
    echo "Deploying with configuration for Docker Compose..."

    # Check the lock before starting ngnix services
    # Note: The lock is controlled by eos-mgm, bootstrap_instance_config.sh
    while [[ -f ${HOST_FOLDER}/cernboxgateway-lock ]]
    do
      echo 'Waiting for the lock to be removed'
      sleep 10
    done

    # Eventually override the certificates with the ones available in certs/boxed.{key,crt}
    if [[ -f ${HOST_FOLDER}/certs/boxed.crt && -f ${HOST_FOLDER}/certs/boxed.key ]]; then
      echo 'Replacing default certificate for HTTPS...'
      /bin/cp "$HOST_FOLDER"/certs/boxed.crt /etc/boxed/certs/boxed.crt
      /bin/cp "$HOST_FOLDER"/certs/boxed.key /etc/boxed/certs/boxed.key
    fi
    ;;

  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac


# Configure nginx with params from the local scope
echo "Configuring nginx with local parameters..."

# Edits on nginx-auth
sed -i -e "s|%%%LDAP_URI_1%%%|${LDAP_URI_1}|" /etc/sysconfig/nginx-auth
sed -i -e "s|%%%LDAP_URI_2%%%|${LDAP_URI_2}|" /etc/sysconfig/nginx-auth
sed -i -e "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /etc/sysconfig/nginx-auth
sed -i -e "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/sysconfig/nginx-auth
sed -i -e "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/sysconfig/nginx-auth

# Edits on nginx.blender.conf.template
sed -i -e "s|%%%EOS_MGM_ALIAS%%%|${EOS_MGM_ALIAS}|" /root/nginx/nginx.blender.conf.template
sed -i -e "s|%%%EOS_MGM_ALIAS_PORT%%%|${EOS_MGM_ALIAS_PORT}|" /root/nginx/nginx.blender.conf.template

# Use a (slightly) different configuration file for nginx if the CERNBOX_SUBDOMAINS is set
if [ "$CERNBOX_SUBDOMAIN" ]; then
  echo "Configuring CERNBox subdomain: $CERNBOX_SUBDOMAIN"
  /bin/cp /root/nginx/nginx.eos.conf.template_cboxsubdomain /root/nginx/nginx.eos.conf.template
  sed -i -e "s|%%%CERNBOX_SUBDOMAIN%%%|${CERNBOX_SUBDOMAIN}|" /root/nginx/nginx.eos.conf.template
else
  /bin/cp /root/nginx/nginx.eos.conf.template_plain /root/nginx/nginx.eos.conf.template
fi

# Edits on nginx.eos.conf.template
if [ `echo $ESCAPE_LANDING_PAGE | tr '[:upper:]' '[:lower:]'` = "true" ]; then
  echo "Dropping landing page. Web traffic is redirected directly to cernbox..."
  sed -i -e "s|root /var/www/html/welcome;|return 301 https://EOS_NGINX_HOSTNAME:EOS_NGINX_LDAP_SSL_PORT/cernbox;|" /root/nginx/nginx.eos.conf.template
fi
DNS_SERVERs=$(grep 'nameserver' /etc/resolv.conf | grep -v "^#" | awk '{print $2}'| tr '\n' ' ')
sed -i -e "s|%%%RESOLVER_IPs%%%|${DNS_SERVERs}|" /root/nginx/nginx.eos.conf.template
sed -i -e "s|%%%EOS_MGM_ALIAS%%%|${EOS_MGM_ALIAS}|" /root/nginx/nginx.eos.conf.template
sed -i -e "s|%%%EOS_MGM_ALIAS_PORT%%%|${EOS_MGM_ALIAS_PORT}|" /root/nginx/nginx.eos.conf.template
sed -i -e "s|%%%SWAN_BACKEND%%%|${SWAN_BACKEND}|" /root/nginx/nginx.eos.conf.template
sed -i -e "s|%%%SWAN_BACKEND_PORT%%%|${SWAN_BACKEND_PORT}|" /root/nginx/nginx.eos.conf.template
sed -i -e "s|%%%OWNCLOUD_BACKEND%%%|${OWNCLOUD_BACKEND}|" /root/nginx/nginx.eos.conf.template
sed -i -e "s|%%%OWNCLOUD_BACKEND_PORT%%%|${OWNCLOUD_BACKEND_PORT}|" /root/nginx/nginx.eos.conf.template

# /etc/sysconfig/nginx --> This will edit /etc/nginx/nginx.eos.conf
echo "CONFIG: HTTP port is ${HTTP_PORT}"
echo "CONFIG: HTTPS port is ${HTTPS_PORT}"
echo "CONFIG: Hostname is ${HOSTNAME}"
sed -i -e "s|EOS_NGINX_HOSTNAME=.*$|EOS_NGINX_HOSTNAME=${HOSTNAME}|" /etc/sysconfig/nginx				# This is the 'server_name' -- Line 36
sed -i -e "s|EOS_NGINX_PLAIN_HTTP_PORT=.*$|EOS_NGINX_PLAIN_HTTP_PORT=${HTTP_PORT}|" /etc/sysconfig/nginx		# Port for plain HTTP traffic (rewrtire rule to HTTPS) -- Line 62
sed -i -e "s|EOS_NGINX_LDAP_SSL_PORT=.*$|EOS_NGINX_LDAP_SSL_PORT=${HTTPS_PORT}|" /etc/sysconfig/nginx			# Port for / and all other redirects -- Line 59
sed -i -e "s|EOS_NGINX_CLIENT_SSL_PORT=.*$|EOS_NGINX_CLIENT_SSL_PORT=${WEBDAV_CLIENT_CERT_PORT}|" /etc/sysconfig/nginx	# Port for ^/cernbox/webdav(.*) and ^/internal_redirect/(.*):(.*?)/(.*) -- Line 47 -- NOT USED

# Apply the configuration
/etc/sysconfig/nginx-auth
/etc/sysconfig/nginx
/etc/sysconfig/nginx-blender

# Apply the customization script (if required)
if [ "$CUSTOMIZATION_REPO" ]; then
  CUSTOMIZATION_PATH="/tmp/customization"
  mkdir -p $CUSTOMIZATION_PATH
  echo "Fetching customizations from $CUSTOMIZATION_REPO"
  git config --global http.sslVerify false
  git clone $CUSTOMIZATION_REPO $CUSTOMIZATION_PATH
  cd $CUSTOMIZATION_PATH
  # Checkout specific commit, if set
  if [ "$CUSTOMIZATION_COMMIT" ]; then
    echo "Checkout commit $CUSTOMIZATION_COMMIT"
    git checkout $CUSTOMIZATION_COMMIT
  fi
  # Run the customization script
  if [ -z "$CUSTOMIZATION_SCRIPT" ]; then
    export CUSTOMIZATION_SCRIPT="entrypoint.sh"
  fi
  echo "Applying customizations via $CUSTOMIZATION_SCRIPT..."
  sh $CUSTOMIZATION_SCRIPT
  if [ $? -ne 0 ]; then
    echo "WARNING: Something went wrong with the customization script."
    echo "WARNING: Configuration might be inconsistent."
  fi
  cd /
fi

# Give control to supervisord
echo "Starting processes..."
# This should be moved to supervisord when base image is cc7
/usr/sbin/uwsgi --daemonize --ini /etc/uwsgi.ini --master-as-root
/usr/bin/supervisord -n -c /etc/supervisord.conf

