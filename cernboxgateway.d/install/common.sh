######################################################################
# PREVENT ACCIDENTAL REINSTALL OF A DIFFERENT SERVICE TYPE ON THE HOST 

function init_check() {
    mkdir -p /etc/cbox || exit -1

    set DIR=$1
    set SCRIPT=$2

    installed_script=`readlink -f /etc/cbox/installed_script`

    if [ -n "$installed_script" ]; then
     installed_script=`basename $installed_script`
    fi
    
    echo this script: $SCRIPT
    
    if [[ -L /etc/cbox/installed_script ]]; then
    
    echo installed script: $installed_script
    
    if [[ $SCRIPT != $installed_script ]]; then
      echo "ERROR: this host has been previously installed as <$installed_script>:   "`ls -l /etc/cbox/installed_script`
      exit -1
    fi
    else
     ln -s  $SCRIPT /etc/cbox/installed_script
    fi

    # GLOBAL VARIABLES 
    CONFIG_SRC_DIR=${DIR}/${SCRIPT}.config.d

    # CUSTOM LOCATIONS
    # configuration files with secrets such as passwords
    # check: ./private.example directory
    CONFIG_PRIVATE_DATA_DIR=${DIR}/private.example

    # location of internal git repositories
    GIT_REPO_DIR=/b/git

    # GLOBAL BEHAVIOUR
    set -o noclobber # Dont override existing files with ">" redirects
    set -o errexit # bail out on all errors immediately
    
    set -o verbose # Echoes all commands before executing.
    
}









