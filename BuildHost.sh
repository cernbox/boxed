#!/bin/bash
# Configure the local host starting from Docker files and having access to CERN GitLab


### Extra variables and functions for development only ###
# Variables and functions in common with the image-based deployment are sourced from uboxed/etc/common.sh

# ----- Variables ----- #
export BUILD_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"	# This is the folder from where this scripts runs
									# Needed as context by docker-compose.override.yml file
DOCKCOMP_YAML_MAIN="uboxed/docker-compose.yml"
DOCKCOMP_YAML_OVERRIDE="docker-compose.override.yml"


### EOS STORAGE
# Version
# NOTE: The base OS for EOS AQUAMARINE is CERN SLC6, while for CITRINE is CERN CC7.
# It is recommended to pin EOS and XROOTD versions to
#        AQUAMARINE: 
#		EOS:    0.3.231
#               XRD:    3.3.6
#        CITRINE:
#               EOS:    
#               XRD:  
# TODO: We should provide one image base on aquamarine and one based on citrine
#	to let the user choose

# TODO: This should go away
#EOS_SUPPORTED_VERSIONS=(AQUAMARINE CITRINE)
#EOS_CODENAME="AQUAMARINE"	# Pick one among EOS_SUPPORTED_VERSIONS
#EOS_CODENAME="CITRINE"

# TODO: We should instead double the variables for version on AQUAMARINE || CITRINE
EOS_VERSION="0.3.231"		# If left empty, the latests version will be installed
XRD_VERSION="3.3.6"
# And also build two images


### EOS FUSE-MOUNT
# Do you want to pint the version of the eos-fuse client?
#FUSE_EOS_VER="4.1.14"
#FUSE_XRD_VER="4.5.0"



# ----- Functions ----- #
# Check to have the subfolder with GitHub repo content
# Needed for functions and variables in common with the image-based deployment 
pull_github_subrepo()
{
echo ""
echo "Pulling latest version of uboxed submodule..."
git submodule init
#git submodule update --remote
}

# Check to have certificates for HTTPS when building images
need_certificates_while_building()
{
if [[ -f secrets/boxed.crt && -f secrets/boxed.key ]]; then
	echo "I have certificates for HTTPS."
else
	echo "Need key and certificate for HTTPS in secrets/boxed.{key,crt}. Cannot continue."
	exit 1
fi
}

# Check the requirements for JupyterHub
jupyterhub_requirements()
{
if [[ -f jupyterhub.d/adminslist ]]; then
	echo "I have the adminlist for JupyterHub."
else
	echo "Need usernames for admins, one per line, in jupyterhub.d/adminslist"
	exit 1
fi
}

# Pin the software version and prepend the dash
pin_software_version()
{
if [[ ! -z "$1" ]]; then
        echo "-"$1
else
	echo ""
fi
}

# Get the latest version of CERN customizations for JupyterHub from GitLab
pull_jupyterhub_customizations()
{
echo ""
echo "Fetching CERN customizations for JupyterHub..." 

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# See: http://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
if [ -d jupyterhub.d/jupyterhub-dmaas ]; then
        cd jupyterhub.d/jupyterhub-dmaas
        git pull
        cd $CWD
else
        git clone https://:@gitlab.cern.ch:8443/dmaas/jupyterhub.git jupyterhub.d/jupyterhub-dmaas
fi
if [ -d jupyterhub.d/jupyterhub-puppet ]; then
        cd jupyterhub.d/jupyterhub-dmaas
        git pull
        cd $CWD
else
        git clone https://:@gitlab.cern.ch:8443/ai/it-puppet-hostgroup-dmaas.git jupyterhub.d/jupyterhub-puppet
fi  
}



### CODE ###

# Fetch image-based deployment subrepo
pull_github_subrepo
source uboxed/etc/common.sh	# ... and import variables and functions from it

# Pin the software versions
# eos storage
#unset EOS_VERSION XRD_VERSION
EOS_VERSION=$(pin_software_version $EOS_VERSION)
XRD_VERSION=$(pin_software_version $XRD_VERSION)
EOS_DOCKERF=`echo $EOS_CODENAME | tr '[:upper:]' '[:lower:]'`.Dockerfile

# eos-fuse
export FUSE_EOS_VERSION=$(pin_software_version $FUSE_EOS_VER)
export FUSE_XRD_VERSION=$(pin_software_version $FUSE_XRD_VER)



# Preliminary Checks
echo ""
echo "Preliminary checks..."
need_root
need_certificates_while_building
jupyterhub_requirements
check_eos_codename
check_required_services_are_available
warn_about_software_requirements
warn_about_interfence_eos_cvmfs

# Clean-Up
check_single_user_container_running
stop_and_remove_containers $DOCKCOMP_YAML_MAIN
cleanup_folders_for_fusemount
initialize_folders_for_fusemount

# Preparation
docker_network
volumes_for_eos
volumes_for_ldap
volumes_for_cernbox

fetch_singleuser_notebook_image
check_ports_availability
set_the_locks


### Build images and other developement stuff ###
# Clone the repo with customizations for JupyterHub
pull_jupyterhub_customizations

# Set the permissions for LDAP/PAM configuration files
chmod 600 ldappam.d/nslcd.conf

# Build the Docker image for EOS storage
echo ""
echo "Building the Docker image for EOS storage..."
docker build -t eos-storage -f eos-storage.d/$EOS_DOCKERF --build-arg EOS_VERSION=${EOS_VERSION} --build-arg XRD_VERSION=${XRD_VERSION} .

# Build and run via Docker Compose
echo ""
echo "Building..."
docker-compose -f $DOCKCOMP_YAML_MAIN -f $DOCKCOMP_YAML_OVERRIDE build
docker-compose -f $DOCKCOMP_YAML_MAIN -f $DOCKCOMP_YAML_OVERRIDE up -d

echo
echo "Configuring..."
while [[ -f "$HOST_FOLDER"/usercontrol-lock ]]
do
	sleep 5 
done

echo ""
echo "Done!"

echo ""
echo "Access to log files: docker-compose -f $DOCKCOMP_YAML_MAIN logs -f"
echo "Or get them sorted in time: docker-compose -f $DOCKCOMP_YAML_MAIN logs -t | sort -t '|' -k +2d"
echo "--> Please source the uboxed/etc/common.sh file first! <--"



### OLD -- DEPRECATED DEPLOYMENT OF EOS STORAGE ###

# NOT WORKING AS WE NEED LDAP TO RETRIEVE ADMIN INFO BUT LDAP IS NOT READY YES AT THIS STAGE
# We resort to the controller container that manages EOS storage deployment and dies  right after.
# It is possible to uncomment this part and forget about the controller container if LDAP is always-on
# or an external LDAP is used
: '
# Check to have (or create) volumes for EOS data and metadata, i.e., make storage persistent
echo ""
echo "Setting up EOS storage..."
EOS_MGM=$EOSSTORAGE_HEADING$EOSSTORAGE_MGM
EOS_MQ=$EOSSTORAGE_HEADING$EOSSTORAGE_MQ

# Set up EOS containers to achieve specific roles, e.g., MGM, MQ, FST.
echo "Instantiating MQ node..."
docker volume inspect $EOS_MQ >/dev/null 2>&1 || docker volume create --name $EOS_MQ
docker run -di --hostname $EOS_MQ.demonet --name $EOS_MQ --net=demonet eos-storage
docker exec -i $EOS_MQ bash /eos_mq_setup.sh

echo "Instantiating MGM node..."
docker volume inspect $EOS_MGM >/dev/null 2>&1 || docker volume create --name $EOS_MGM
docker run -di --hostname $EOS_MGM.demonet --name $EOS_MGM --net=demonet eos-storage
docker exec -i $EOS_MGM bash /eos_mgm_setup.sh

echo "Instantiating FST nodes..."
for i in {1..6}
do   
    EOS_FST=$EOSSTORAGE_HEADING$EOSSTORAGE_FST_NAME$i
    docker volume inspect $EOS_FST >/dev/null 2>&1 || docker volume create --name $EOS_FST
    docker run --privileged -di --hostname $EOS_FST.demonet --name $EOS_FST --net=demonet eos-storage
    docker exec -i $EOS_FST bash /eos_fst_setup.sh $i
done

echo "Configuring the MGM node..."
echo "\t --> Booting the FS..."
docker exec -i $EOS_MGM bash /eos_mgm_fs_setup.sh

echo "\t --> Installing and running nscd and nslcd services..."
docker exec -i $EOS_MGM yum -y install nscd nss-pam-ldapd
docker cp ./ldappam.d/nscd.conf $EOS_MGM:/etc
docker cp ./ldappam.d/nslcd.conf $EOS_MGM:/etc
docker cp ./ldappam.d/nsswitch.conf $EOS_MGM:/etc

docker exec -i $EOS_MGM nscd
docker exec -i $EOS_MGM nslcd 

echo "\t --> Configuring the EOS namespace..."
docker cp eos-storage/configure_eos_namespace.sh $EOS_MGM:/configure_eos_namespace.sh
docker exec -i $EOS_MGM bash /configure_eos_namespace.sh
'
