#! /bin/bash

# ----- Check the lock before configuring EOS ----- #
# Note: The lock is controlled by ldap-ldapadd. 
#	It is needed to have the LDAP server configured before starting with EOS-Storage deployment.
while [[ -f ${HOST_FOLDER}/eos-storage-lock ]]
do
  echo 'Waiting for the lock to be removed'
  sleep 10
done


if [ -z ${EOS_STORAGE_IMAGE_NAME} ]; then
  echo "WARNING: eos-storage image name not set."
  echo "Defaulting to gitlab-registry.cern.ch/cernbox/boxedhub/eos-storage:latest"
  EOS_STORAGE_IMAGE_NAME="gitlab-registry.cern.ch/cernbox/boxedhub/eos-storage:latest"
fi


# ----- Variables ----- #
LDAP_ENDPOINT="ldap.demonet"
NETNAME="demonet"
EOS_MGM="eos-mgm"
EOS_MQ="eos-mq"
EOS_FST="eos-fst"
FST_USERDATA_DIR="/home/fst_userdata"
#EOS_FST_VOLUME="/home/eos"
#EOS_FST_DATADIR=${EOS_FST_VOLUME}"/fst"	# Note: The FST will append $FST_NUMBER (in this case {1..6}) to $EOS_FST_DATADIR
						#	and the actual write directory will be, e.g., "/home/eos/fst1"
						# We store "/home/eos" in a Docker volume to provide persistent storage

# ----- Configuring EOS Storage ----- #
# Note: The line modifying /etc/hosts is needed to remove IPv6 entries as they cause an erratic behavior with EOS Aquamarine
# `docker exec -i <container_name> bash -c 'cp /etc/hosts /etc/hosts.backup && grep -v "ip6-" /etc/hosts.backup > /etc/hosts'`
echo "Instantiating MQ node..."
docker run --cap-add SYS_PTRACE -di -v $EOS_MQ:/var/eos -h $EOS_MQ.$NETNAME --name $EOS_MQ --net=$NETNAME ${EOS_STORAGE_IMAGE_NAME} bash
docker exec -i $EOS_MQ bash -c 'cp /etc/hosts /etc/hosts.backup && grep -v "ip6-" /etc/hosts.backup > /etc/hosts'
docker exec -i $EOS_MQ bash /root/eos_setup/eos_mq_setup.sh


echo "Instantiating MGM node..."
docker run --cap-add SYS_PTRACE -di -v $EOS_MGM:/var/eos -h $EOS_MGM.$NETNAME --name $EOS_MGM --net=$NETNAME -e "EOS_MGM_MASTER1=$EOS_MGM.$NETNAME" -e "EOS_MGM_MASTER2=$EOS_MGM.$NETNAME" -e "EOS_MGM_ALIAS=$EOS_MGM.$NETNAME" -e "EOS_MQ_ALIAS=$EOS_MQ.$NETNAME" -e "LDAP_ENDPOINT=$LDAP_ENDPOINT" ${EOS_STORAGE_IMAGE_NAME} bash
docker exec -i $EOS_MGM bash -c 'cp /etc/hosts /etc/hosts.backup && grep -v "ip6-" /etc/hosts.backup > /etc/hosts'
docker exec -i $EOS_MGM bash /root/eos_setup/eos_mgm_setup.sh

echo "Instantiating FST nodes..."
for i in {1..6}
do   
  FSTHOSTNAME=${EOS_FST}$i
  #docker run --privileged -di -v $FSTHOSTNAME:/var/eos -v $FSTHOSTNAME"_userdata":$EOS_FST_VOLUME -h $FSTHOSTNAME.$NETNAME --name $FSTHOSTNAME --net=$NETNAME -e "EOS_MGM_ALIAS=$EOS_MGM.$NETNAME" -e "EOS_MQ_ALIAS=$EOS_MQ.$NETNAME" -e "EOS_FST_NUMBER=$i" -e "EOS_DATADIR=$EOS_FST_DATADIR" ${EOS_STORAGE_IMAGE_NAME} bash
  docker run --privileged -di -v $FSTHOSTNAME:/var/eos -v $FSTHOSTNAME"_userdata":$FST_USERDATA_DIR -h $FSTHOSTNAME.$NETNAME --name $FSTHOSTNAME --net=$NETNAME -e "EOS_MGM_ALIAS=$EOS_MGM.$NETNAME" -e "EOS_MQ_ALIAS=$EOS_MQ.$NETNAME" -e "EOS_FST_NUMBER=$i" -e "FST_USERDATA_DIR=$FST_USERDATA_DIR" ${EOS_STORAGE_IMAGE_NAME} bash
  docker exec -i $FSTHOSTNAME bash -c 'cp /etc/hosts /etc/hosts.backup && grep -v "ip6-" /etc/hosts.backup > /etc/hosts'
  docker exec -i $FSTHOSTNAME bash /root/eos_setup/eos_fst_setup.sh
done

echo "--> Running nscd and nslcd services..."
docker exec -i $EOS_MGM sed -i "s/%%%LDAP_ENDPOINT%%%/${LDAP_ENDPOINT}/" /etc/nslcd.conf
docker exec -i $EOS_MGM nscd
docker exec -i $EOS_MGM nslcd 

echo "--> Configuring the MGM node..."
docker exec -i $EOS_MGM bash /root/configure_eos_namespace.sh

# ----- Unlocking other services ----- #
echo "Unlocking eos-fuse client."
rm ${HOST_FOLDER}/eos-fuse-lock
echo "Unlocking cernbox."
rm ${HOST_FOLDER}/cernbox-lock
echo "Unlocking cernboxgateway."
rm ${HOST_FOLDER}/cernboxgateway-lock

# ----- Goodbye ----- #
echo "I'm done. Exiting..."
