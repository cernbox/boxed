## Change Log
-----

## eos-storage
#### *v0.8*
* 2018 08 13: Dedicated script to remove locks. The script is called by the `bootstrap_instance_config.sh` for new a new deployment, or it is started by supervisor for alread-configured EOS clusters
* 2018 08 13: Set 1M inodes as quota for the recycle bin
* 2018 08 08: `eos recycle config --size` is deprecated. Use `eos quota set -g 99` instead.
* 2018 08 08: Additional lock for FSTs with docker-compose. They need the MGM to be up and configured before attaching the filesystems.
* 2018 08 08: Define startup actions when deploying with docker-compose (deprecates eos-controller container)

#### *v0.7*
* 2018 07 09: Setup crond and logrotate for EOS logs
* 2018 07 09: Remove yum-autoupdate
* 2018 07 04: Update to EOS 4.2.26 and xrootd 4.8.3
* 2018 07 04: Update of base image to CERN CC 7.5 -- cc7-base:20180516

#### *v0.6*
* 2018 05 07: Update start script to support custom LDAP parameters

#### *v0.5*
* 2018 04 12: Update to EOS 4.2.18 and xrootd 4.8.1

#### *v0.4*
* 2018 04 04: Register a-priori CERNBox Web container via domain name provided by Kubernetes service (cernbox.cernbox.boxed.svc.cluster.local). Avoids permission issues when the CERNBox Web container presents itself via the Kubernetes service name on the EOS and not with the IP address. This depends on the deployment context and it is hard to predict in advance.
* 2018 03 29: For FST, dropping "EOS_FST_NUMBER" environment variable. The FST number is automatically inferred during the fs registation process.
* 2018 03 29: For FST, renaming of "FST_USERDATA_DIR" environment variable in "FST_MOUNTPOINT".
* 2018 03 29: Automate FST register in eos cluster. Support of geotab and scheduling group via dedicated environment variables, "FST_GEOTAG" and "FST_SCHEDULING_GROUP" respectively.
* 2018 03 29: For MGM, automate default 'default' space and 'default.0' group by default.
* 2018 03 29: Automate MGM configuration on first run via `bootstrap_instance_config.sh` script
* 2018 03 29: Make xrootd // eos processes managed by supervisord and run in foreground.
* 2018 03 29: Update of base image to cc7-base:20180112.

#### *v0.3*
* 2017 12 21: Force the xroot daemon to start on IPv4 protocol stack only. Useful for buggy network stacks.
* 2017 11 30: Install net-tools and bind-utils on eos-storage image.
* 2017 11 30: Check network configuration before starting EOS. Sanity check on the ability to resolve host FQDN -- IP address either way.
* 2017 11 30: Remove eos-testkeytab from installation. Provide pre-defined keytabs `eos.keytab`, `eos-server.keytab`, `eos-gateway.keytab` for mgm, fst, gateways. All the three keytabs are stored on eos-storage Docker image in `/etc`.
* 2017 11 30: Provide xrootd configuration files for mq and sync, respectively `xrd.cf.mq` and `xrd.cf.sync`.
* 2017 11 29: Set lifetime of files in recycle bin to 30 days.
* 2017 11 29: Automatically starts mq process if `EOS_MGM_ALIAS` and `EOS_MQ_ALIAS` are identical.
* 2017 11 29: Better configuration for eos-mgm:
  * Configuration of eos headnode, namespace, and trusted gateways is modular and demanded to `set_basics.sh`, `set_namespace.sh`, and `set_gateways.sh`, respectively. In the case of deployment with kubernetes, eos-mgm configuration is stored on persistent storage volumes and the three scripts are executed only when bootstrapping the instance for the first time. 
  * Trusted gateways configuration depends on the deloyment context. For docker-compose, a static configuration is assumed with cernbox.demonet and eos-fuse.demonet being unix gateways and cernboxgateway.demonet being a https gateway. For kuberntes, only cernbox.cernbox.boxed.svc.cluster.local (i.e., cernbox OC backend) is trusted and is associated to a kubernetes service. Configuration for the eos-fuse daemons and the cernbox ngnix gateway is subject to change and requires dynamic configuration on the eos side.

#### *v0.2*
* 2017 11 07: Explicitly installing leveldb and zeromq on eos-storage Docker image.
* 2017 11 07: Start the eos configuration script automatically upon deployment.

#### *v0.1*
* 2017 10 26: Install and load libjemalloc on EOS Citrine.
* 2017 10 26: Backup and restore subfolders and files in `/var/eos` and `/var/log/eos` if such folders are mapped to persistent volumes (e.g., hostPath on Kuberentes).
* 2017 10 26: Update of `eos-storage-{mq, mgm, fst.template.yaml}` to store server configuration, user data, and logs on persistent volumes. Logs should be stored on local host folders, while server configuration, namespace, and user data should be stored on external persistent volumes (e.g., OpenStack Cinder volumes).

#### *v0*
* 2017 10 25: Collapsing script `eos_mgm_fs_setup.sh` into `configure_eos_namespace.sh`. Affects also *eos-controller:latest*.
* 2017 10 25: Removing subdir "fstN" for volumes attached to FSTs where userdata are stored. Affects also *eos-controller:latest* and mount-point descriptors for Kubernetes. 
Hidden files `.eosfsid` and `.eosfsuuid` are stored in the top-level directory and used as markers to detect if the volume already contains eos data. 
  * FSID is a 12-digit long string equal to $EOS_FST_NUMBER padded with trailing zeros
  * FSUUID format: 8-"EOSxxFST", 4-"xDOC", 4-"KERx", 4-random string lowercase+digits, 12-$FSID



## eos-fuse
#### *v0.6*
* 2018 07 09: Install supervisor for nscd, nslcd, and crond
* 2018 07 09: Setup crond and logrotate for EOS fuse logs
* 2018 07 09: Remove yum-autoupdate
* 2018 07 09: Update to EOS 4.2.26 and xrootd 4.8.3
* 2018 07 09: Update of base image to CERN CC 7.5 -- cc7-base:2018051

#### *v0.5*
* 2018 05 07: Update start script to support custom LDAP parameters

#### *v0.4*
* 2018 04 12: Update to EOS 4.2.18 and xrootd 4.8.1
* 2018 04 12: Update of base image to cc7-base:20180316

#### *v0.3*
* 2018 03 14: Update of software version: eos-fuse: 4.2.16, xrootd: 4.8.1
* 2018 02 21: Renaming "EOS_MGM_ENDPOINT" into "EOS_MGM_ALIAS" to be consistent with other params referring to the EOS MGM alias.
* 2018 02 21: Update of base image to cc7-base:20180112
* 2018 02 21: Update of software version: eos-fuse: 4.2.13, xrootd: 4.8.1

#### *v0.2*
* 2017 12 01: Automate gateway addition and removal via scripts `config_gateway.sh` from `eos-storage.citrine.d/utils/`.
* 2017 12 01: Provisioning of `stop.sh` script for eos-fuse Docker images in order to run commands right before container is removed. This is used in Kubernetes to stop the eosd service (which results in cleaning the /eos mount point on the node) and remove the gateway from eos.
* 2017 12 01: New environment variables "EOS_GATEWAY_SELF_REGISTRATION" and "EOS_GATEWAY_AUTH" to automate gateway addition and removal on start and exit, respectively.
  * _Affects also Kubernetes yaml file `SWAN.yaml`._
* 2017 11 30: Copy pre-definied `eos-gateway.keytab` from `eos-storage.citrine.d/keytabs` in eos-fuse Docker image.

#### *v0.1*
* 2017 10 23: Stabilizing software versions ad polishing Dockerfile



## eos-controller (for docker-compose only)
## WARNING: eos-controller is deprecated since 07/08/2018 -- fed2c857cb5cd6660d61460d1ae762e08c442eb4
##          All the eos-storage containers are self-configuring also when deploying with docker-compose
#### *latest*
* 2017 10 25: Fixing LDAP enpoint name replacement (`docker exec -i $EOS_MGM sed -i "s/%%%LDAP_ENDPOINT%%%/${LDAP_ENDPOINT}/" /etc/nslcd.conf`) for eos-mgm.
* 2017 10 25: Collapsing script `eos_mgm_fs_setup.sh` into `configure_eos_namespace.sh`. Affects also *eos-storage:v0*.
* 2017 10 25: Removing subdir "fstN" for volumes attached to FSTs where userdata are stored. Affects also *eos-storage:v0* and mount-point descriptors for Kubernetes. Hidden files `.eosfsid` and `.eosfsuuid` are stored in the top-level directory and used as markers to detect if the volume already contains eos data. 
  * FSID is a 12-digit long string equal to $EOS_FST_NUMBER padded with trailing zeros
  * FSUUID format: 8-"EOSxxFST", 4-"xDOC", 4-"KERx", 4-random string lowercase+digits, 12-$FSID



## cernbox
#### *v0.9*
* 2018 08 01: cernsso authentication type is depracated. Configuration files are remove. Use customization repo instead to define how AuthN & AuthZ should be managed
* 2018 07 11: Setup crond and logrotate for httpd, shibd, owncloud logs
* 2018 07 11: Remove yum-autoupdate
* 2018 07 11: Update of base image to CERN CC 7.5 -- cc7-base:20180516

#### *v0.8*
* 2018 06 08: Handle user names with special characters that get base64-encoded in LDAP
* 2018 05 30: Fix for `httpd.d/shibd.conf` to have the WebDAV endpoint required to mount CERNBox via davfs not protected by SSO. We fall back on basic authentication with username and password
* 2018 05 24: Inspect return code of ldapsearch function to avoid clashes on internal uid when using UserBackendSSOtoLDAP backend
* 2018 05 23: Properly decode unicode character when using displaying user name through a custom UserBackend
* 2018 05 16: Undo persisify CERNBox config in `/var/www/html/cernbox/config/`. The configuration file is built dinamically everytime the container starts. There is no point in storing it, as it would be overwritten anyway
* 2018 05 16: New environment variable "ADMINS_LIST" to define list of CERNBox users with administrator privileges
* 2018 05 16: Store `/var/www/html/cernbox/config/` on peristent storage. Required in case custom configurations are applied directly to the `config.php` file (e.g., admins list).
* 2018 05 16: Fix for `httpd.d/shibd.conf` to have the endpoints required by the Moodle integration plugin not protected by SSO
* 2018 05 16: Fix for `httpd.d/shibd.conf` to have the CERNBox applications (rootviewer, swanviewer, etc.) not protected by SSO. Allows for visualization of a shared notebook without being authenticated
* 2018 05 07: Proper parser to forward SSO user metadata to bash script for LDAP entry creation with "UserBackendSSOtoLDAPNumericUID"
* 2018 05 07: Support for custom LDAP parameters for user backend "UserBackendSSOtoLDAPNumericUID"
* 2018 05 07: Proper parser to forward SSO user metadata to bash script for LDAP entry creation with "UserBackendSSOtoLDAP"
* 2018 05 07: Custom user backend may require to add additional entries in the LDAP server. New environment varialbes are passed to bind to LDAP with admin privileges: "LDAP_ADMIN_BIND_DN", "LDAP_ADMIN_BIND_PASSWORD", "LDAP_ENTRY_NAME_PREFIX", "LDAP_ENTRY_UIDNUMBER_START"
* 2018 05 07: Support for custom LDAP parameters for user backend "UserBackendSSOtoLDAP"
* 2018 05 07: New environment variables "LDAP_URI" (replaces "LDAP_ENDPOINT"), "LDAP_PORT", "LDAP_BASE_DN", "LDAP_BIND_DN", "LDAP_BIND_PASSWORD"
* 2018 05 07: Update start script, create users' home script, and CERNBox config file to support custom LDAP parameters

#### *v0.7*
* 2018 04 09: Bugfix for 'get_display_name.sh' script when using WebIdentityHandlers
* 2018 03 23: Update of base image to cc7-base:20180316
* 2018 03 23: Adding scripts to store Web identity with numerical uid that can be used directly as uid/gid in the Unix world. Scripts in '/var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAPNumericUID.d' and user backend at '/var/www/html/cernbox/lib/private/CernBox/Backends/UserBackendSSOtoLDAPNumericUID.php'. Requires 'ldap:v0.1'.
* 2018 03 12: Renaming 'HOSTNAME' into 'CERNBOXGATEWAY_HOSTNAME' for 'shibboleth2.xml.template' file to be consistent with other config files using the same variable.
* 2018 03 12: Adding scripts to manage Web to Unix identity creation and mapping in '/var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAP.d' and user backend at '/var/www/html/cernbox/lib/private/CernBox/Backends/UserBackendSSOtoLDAP.php'. Requires 'ldap:v0.1'.
* 2018 03 12: Decoupling CERN SSO integration with other Shibboleth-based authentication methods. Authentication via CERN SSO can be now enforced setting the environment variable "AUTH_TYPE" to "cernsso". For any other shibboleth-based authentication, please set "AUTH_TYPE" to "shibboleth" and provide the required configuration files via the customization script.

#### *v0.6*
* 2018 02 26: Ability to self-register as quota administrator on EOS. New environment variable "EOS_QUOTAADMIN_SELF_REGISTRATION".
* 2018 02 22: EOS keytab can be passed as kubernetes secret and loaded by specifying the keytab path via new environment variable "EOS_GATEWAY_KEYTAB_FILE".
* 2018 02 20: EOS params are passed as environment variables instead of being hard coded. New environment variables "EOS_PREFIX", "EOS_METADATADIR", "EOS_RECYCLEDIR", "EOS_PROJECTPREFIX".

#### *v0.5*
* 2018 02 14: Ability to customize the container by pulling a github repo and running a script. New environment variables "CUSTOMIZATION_REPO", "CUSTOMIZATION_COMMIT", and "CUSTOMIZATION_SCRIPT".
* 2018 02 09: Install python and required packages for SWAN viewer in CERNBox
* 2018 02 01: Add applications to CERNBox -- SWANviewer, ROOTviewer, eosinfo (disabled by default)
* 2018 02 01: Pick the correct branch for CERNBox apps

#### *v0.4*
* 2017 12 21: Default user quota is 10GB instead of 1TB.
* 2017 12 19: Do not create users folders on bootstrap but only upon user login.
* 2017 12 18: Adding supervisord and its modular configuration to manage processes running in cernbox container.
* 2017 12 13: Adding file editor application to CERNBox.
* 2017 12 04: New environment variable "%%%DATABASE_BACKEND%%%" to select database backend for CERNBox. Possible options are "mysql" or "sqlite". If left undefined, the default choice is "sqlite". The actual database choice is enforce at run time via `start.sh` script. The configuration parameters for the two databases are provided in `cernbox.config.mysql` and `cernbox.config.sqlite`. The deployment with mysql backend requires an additional docker container running MariaDB. This is forseen only in the context of a Kubernetes deployment, while for docker-compose only sqlite can be used.
  * _Affects also Kubernetes yaml files `cbox-backend.yaml` and `CERNBOX.yaml`._
  * _Defines new Docker image `cernboxmysql`._
* 2017 12 04: Improved cernbox configuration file in `/var/www/html/cernbox/config/config.php`. It now sources form the vanilla `cernbox.config` file only `passwordsalt`, `secret`, and `instanceid` parameters + Redis caching is enabled by default + Allows switching between local sqlite database and mysql database via environment variable "%%%DATABASE_BACKEND%%%".
* 2017 12 04: Paramters to connect to MySQL server loaded via Kubernetes secrets.
  * _Affects also Kubernetes yaml files `cbox-backend.yaml` and `CERNBOX.yaml`._

#### *v0.3*
* 2017 11 09: Install oauth2 app on CERNBox Docker image. It is required by the Moodle integration plugin for Up2U.
* 2017 11 09: Start redis with `--daemonize yes` option from command line.

#### *v0.2*
* 2017 11 07: Enabling ability to login via SSO on CERNBox.
* 2017 11 07: Enabling admin permissions to 'dummy_admin' user on CERNBox.
* 2017 11 01: CERNBox backend accepts connections only over SSL only. Listening port can be specified with HTTPS_PORT environment variable. Certificates used by apache are stored in `/etc/boxed/certs/boxed.{crt, key}` and can be replaced at run time with new certificates stored in `${HOST_FOLDER}/certs/boxed.{crt, key}`.

#### *v0.1*
* 2017 10 27: Update of `cbox-backend.yaml` to store shares db (`/var/www/html/cernbox/data`) and httpd logs. Shares database should be stored on external persistent volumes (e.g., OpenStack Cinder volumes).
* 2017 10 27: Backup and restore subfolders and files in `/var/www/html/cernbox/data` if such folders are mapped to persistent volumes (e.g., hostPath on Kuberentes).



## cernboxgateway
#### *v0.8*
* 2018 08 01: Ability to use a subdomain (e.g., cernbox.sciencebox.cern.ch) for CERNBox. This allows to decouple from the main domain where the landing page lives
* 2018 08 01: Update TLS configuration for nginx proxy
* 2018 07 31: New dhparams.pem in `/etc/boxed/certs/dhparams.pem` from secrets/dhparams.pem
* 2018 07 31: Ability to customize the container by pulling a github repo and running a script. New environment variables "CUSTOMIZATION_REPO", "CUSTOMIZATION_COMMIT", and "CUSTOMIZATION_SCRIPT"
* 2018 07 31: New point of contact for Science Box users
* 2018 07 31: Renaming page title to Science Box (was Boxed)

#### *v0.7*
* 2018 07 11: Setup crond and logrotate for nginx
* 2018 07 11: Rebase Dockerfile
* 2018 07 11: Update of base image to CERN CC 6.10 -- slc6-base:20180622

#### *v0.6*
* 2018 05 18: Start-up wrappers to handle nginx failure when upstream servers are not available
* 2018 05 18: Event listener to bail out form supervisor when processes enter FATAL state
* 2018 05 03: New environment variables "LDAP_URI1" (replaces "LDAP_BACKEND1"), "LDAP_URI2" (replaces "LDAP_BACKEND2"), "LDAP_PORT", "LDAP_BASE_DN", "LDAP_BIND_DN", "LDAP_BIND_PASSWORD"
* 2018 05 03: Ability to configure LDAP paramters via environment variables
* 2018 05 03: Update of base image to slc6-base:20180316

#### *v0.5*
* 2018 04 09: Use simple 'daemonize' option (instead of 'daemonize2') to start uwsgi.
* 2018 03 12: Set proxy_set_header directive to maximize compatibility with SSO solutions.
* 2018 02 22: EOS keytab can be passed as kubernetes secret and loaded by specifying the keytab path via new environment variable "EOS_GATEWAY_KEYTAB_FILE".
* 2018 02 20: EOS params are passed as environment variables instead of being hard coded. New environment variable "EOS_PREFIX".

#### *v0.4*
* 2018 02 09: New environment variale "ESCAPE_LANDING_PAGE" to redirect users to CERNBox login, instead of showing the landing page
* 2018 02 02: Add uwsgi and related config in `nginx.eos.conf` to support "Open in SWAN" action from CERNBox

#### *v0.3*
* 2017 12 21: Automate HTTPS gateway registration in EOS with both IPv4 and IPv6 compatibility. Useful for buggy network stacks.
* 2017 12 21: New environment variables "EOS_GATEWAY_SELF_REGISTRATION" and "EOS_GATEWAY_AUTH" to register HTTPS gateway in EOS.
  * _Affects also Kubernetes yaml file `CERNBOX.yaml`._
* 2017 12 19: Fixed empty PUT request for write+delete permission and password protection on cernbox shares.
* 2017 12 15: Adding supervisord to manage processes running in cernboxgateway container.
* 2017 12 15: Fix for resolver directive when using proxy_pass in nginx.
* 2017 12 13: Adding internal redirect on nginx (gateway) for PUT requests to the text editor.
* 2017 11 10: SSL certificates on cernboxgatway now at `/etc/boxed/certs/boxed.{crt, key}`

#### *v0.2*
* 2017 11 06: New base image for cernboxgateway (cern/slc6-base:20170406 --> 20170920).
* 2017 11 02: Environment variable `BOX_HOSTNAME` renamed to `HOSTNAME` for cernboxgateway.
  * _Affects also docker-compose and Kubernetes yaml file `cbox-gateway.yaml`._
* 2017 11 01: CERNBox backend accepts connections only over SSL only. Listening port can be specified with HTTPS_PORT environment variable. Certificates used by apache are stored in `/etc/boxed/certs/boxed.{crt, key}` and can be replaced at run time with new certificates stored in `${HOST_FOLDER}/certs/boxed.{crt, key}`.
* 2017 11 01: Expliciting column in upstream server directive for port specification. E.g., "server %%%EOS_MGM_ALIAS%%%%%%EOS_MGM_ALIAS_PORT%%%;" becomes "%%%EOS_MGM_ALIAS%%%:%%%EOS_MGM_ALIAS_PORT%%%".
  * _Affects also docker-compose and Kubernetes yaml file `cbox-gateway.yaml`._

#### *v0*
* 2017 10 27: Update of `cbox-gateway.yaml` to store nginx logs on local host folder. Requires the pre-creation of `/var/log/nginx/AUTH` and `/var/log/nginx/blender` otherwise the corresponding nginx process will not start. This applies to configuration for Kubernetes only.



## cernboxmysql (for Kubernetes only)
#### *v0.5*
* 2018 02 01: Add applications to CERNBox -- SWANviewer, ROOTviewer, eosinfo (disabled by default)
* 2018 02 01: Pick the correct branch for CERNBox apps

#### *v0*
* 2017 12 19: Pre-population of the MySQL database with CERNBox apps activated.
* 2017 12 11: Streamlining of MySQL preparation for CERNBox. Scripts are provided to pre-populate the MySQL database so that the choice of database type for CERNBox can be done at run time.
* 2017 12 04: New Docker container for MySQL (MariaDB) server. Dockerfile at `cernbox-mysql.Dockerfile` and additional files at `cernbox-mysql.d`. Refer to `CERNBOX.yaml` for resources specifications.



## jupyterhub
#### *v0.9*
* 2018 08 08: Add memory option to spawner template
* 2018 07 20: Disable unsafe protocols and ciphers for https and https+shibboleth
* 2018 07 12: "cernsso" authentication is deprecated and removed in favour of "shibboleth". Further configuration is required and can be achieved leveraging on customization script
* 2018 07 12: JupyterHub configuration files support additional parameters for the SSOAuthenticator (e.g., using sed in the customization script)
* 2018 07 12: "SSOtoLDAPAuthenticator" has the ability to authorize only whitelisted users
* 2018 07 10: Setup crond and logrotate for httpd, shibd, and JupyterHub logs
* 2018 07 10: Rebase Dockerfile
* 2018 07 10: Remove yum-autoupdate
* 2018 07 10: Update of base image to CERN CC 7.5 -- cc7-base:20180516

#### *v0.8*
* 2018 06 03: Renaming env var "EOS_FOLDER" to "EOS_USER_PATH". It must now bring to the folder with the list of users' initials. This allows for the support of different paths on the eos side (e.g., `/eos/user`, `/eos/docker/user`, `/eos/containers/scratch/user`, ...)

#### *v0.7*
* 2018 05 03: New environment variables "LDAP_URI" (replaces "LDAP_ENDPOINT"), "LDAP_PORT", "LDAP_BASE_DN", "LDAP_BIND_DN", "LDAP_BIND_PASSWORD"
* 2018 05 03: Ability to configure LDAP paramters via environment variables 
* 2018 05 03: CERN Kube Spawner moved to CERN customization part in Dockerfile
* 2018 05 03: Pinning Docker version to 17.03.2
* 2018 05 03: Rebasing Dockerfile to install python packages via pip instead of yum

#### *v0.6*
* 2018 04 11: Renaming `jupyterhub.d/WebIdentityHandlers/SSO2LdapAuthenticator` to `jupyterhub.d/WebIdentityHandlers/SSO2toLDAPAuthenticator`.
* 2018 04 09: Moving additional authenticators in subfolder "WebIdentityHandlers"
* 2018 04 09: Installation of "SSO Remote User Authenticator" as additional authenticator for integration with SSO solutions
* 2018 04 03: Update of base image to cc7-base:20180316
* 2018 04 03: Fix for the detection of the IP address to run the Hub when unable to use the FQDN
* 2018 03 14: Renaming "Web to Unix Authenticator" to "SSO to LDAP Authenticator" for consistency with CERNBox User Backend. For instrcutions see `jupyterhub.d/SSO2LdapAuthenticator`.
* 2018 03 13: Ability to use a custom authenticator when paired with "shibboleth" as "AUTH_TYPE". Such authenticator should be installed on the container and be defined in `/srv/jupyterhub/jupyterhub_config.py` by replacing the string "%%%SHIBBOLETH_AUTHENTICATOR_CLASS%%%".
* 2018 03 13: Decoupling CERN SSO integration with other Shibboleth-based authentication methods. Authentication via CERN SSO can be now enforced setting the environment variable "AUTH_TYPE" to "cernsso". For any other shibboleth-based authentication, please set "AUTH_TYPE" to "shibboleth" and provide the required configuration files via the customization script.
* 2018 03 12: Forward REMOTE_USER as HTTP header from Apache
* 2018 03 12: Install Web to Unix Authenticator (see 'jupyterhub.d/Web2UnixAuthenticator')
* 2018 03 12: Install REMOTE_USER Authenticator onboard (https://github.com/cwaldbieser/jhub_remote_user_authenticator)

#### *v0.5*
* 2018 02 20: Force apache to check intermediate certificate file by directive "SSLCertificateChainFile"
* 2018 02 14: Ability to customize the container by pulling a github repo and running a script. New environment variables "CUSTOMIZATION_REPO", "CUSTOMIZATION_COMMIT", and "CUSTOMIZATION_SCRIPT".
* 2018 02 02: Fixes for CERNHandlers to manage notebooks in `/eos/docker/user`
* 2018 02 02: Updates from the upstream customizations for SWAN

#### *v0.4*
* 2017 12 19: Adding supervisord and its modular configuration to manage processes running in jupyterhub container.
* 2017 11 27: Moving configuration files for JupyterHub from `/srv/jupyterhub/jupyterhub_config` to `/root/jupyterhub_config`.
* 2017 11 27: Fixing configuration for JupyterHub with KubeSpawner so that single-user sessions are not killed if the Hub dies.
* 2017 11 16: Development of CERNKubeSpawner (based on CERNSpawner) to spawn single-user containers on the nodes belonging to the Kubernetes cluster. Updates to `jupyterhub.Dockerfile` and to the configuration file of JupyterHub if the deployment context is 'kubespawner'. 
* 2017 11 16: New yaml file for the deployment of SWAN with KubeSpawner being the default option.

#### *v0.2*
* 2017 11 06: Finalizing ability to login via SSO on SWAN.
* 2017 11 02: Installation and configuration of http proxy in front of jupyterhub required for shibboleth-based authentication. It is possible to decide at deployment-time which authentication method should be used (i.e., local LDAP VS Shibboleth). Listening ports of the proxy can be specified with "HTTP_PORT" and "HTTPS_PORT" environment variables. The proxy automatically redirects traffic to HTTPS but requires knowledge on the hostname, which can be specified setting "HOSTNAME" environment variable. Certificates used by apache are stored in `/etc/boxed/certs/boxed.{crt, key}` and can be replaced at run time with new certificates stored in `${HOST_FOLDER}/certs/boxed.{crt, key}`.
* 2017 11 02: Polishing configuration files `jupyterhub_config.{docker, kubernetes}.py`.



## imagepuller
### 2019 02 12 -- Moved to https://gitlab.cern.ch/sciencebox/docker-images/imagepuller
#### *latest*
2019 02 04: Exponential backoff time when pulling fails. Max retries is set to 12, which results in ~1h of maximum backoff time.
2018 01 15: Used only by SWAN in Kubernetes with kubespawner. It pulls single-user container image on the SWAN worker nodes and (dramatically) reduces the time required to spawn the first user session (as the image will be already available locally). In the Kubernetes context, it is deployed on SWAN worker nodes as DaemonSet and, after a successfull pull, sleeps forever to avoid restarting the containers in the pod set.
2018 01 15: New Docker image for imagepuller. It pulls any container image from remote Docker registries by connecting to the docker daemon running on the host. Uses upstream "docker:stable" as parent image.




## cvmfs
#### *v0.5*
* 2018 05 13: Prefectch packages as soon as the container is started, then fall back on the supercronic job
* 2018 05 09: Make sure the squid proxy is up and running before mounting the repositories
* 2018 05 09: Replacing "CVMFS_SQUID_ENDPOINT" environment variable with "CVMFS_SQUID_HOSTNAME" and "CVMFS_SQUID_PORT"

#### *v0.4*
* 2018 03 26: Define supercronic (https://github.com/aptible/supercronic) cronjob to prefetch packages from CVMFS
* 2018 03 23: Provide stop script to clean-up mountpoint on exit
* 2018 03 23: Update of base image to cc7-base:20180316


#### *v0.3*
* 2018 01 17: Prefetch required packages from CVMFS every 15 minutes. This (dramatically) speeds up the session startup time and the time to have the Kernel ready in a notebook by keeping the CVMFS cache hot. Defines new environment variables to specify the `SOFTWARE_STACK` (e.g., "LCG_91") and the `PLATFORM` (e.g. "x86_64-slc6-gcc62-opt") for which pakages must be prefetched.
* 2018 01 15: New environment variables `CVMFS_UPSTREAM_CONNECTION` to select among: a) "direct" -- Direct connection to upstram StratumOne mirror, b) "cern" -- Connection to CERN proxies (works only within CERN), c) "squid" -- Connection to cluster-wise squid proxy (see note on cvmfs-squid). Option c) "squid" requires additional parameter `CVMFS_SQUID_ENDPOINT` to define the URL of the cluster squid proxy. Default is "http://cvmfssquid.boxed.svc.cluster.local:3128".
* 2018 01 15: Dropped configuration lines for local squid proxy.

#### *v0.2*
* 2017 11 16: New configuration for CVMFS cache. Disabled local squid proxy, Enabled geographical awareness, Cache size set to 10GB, Set correct mount point options.

#### *v0.1*
* 2017 11 06: Removal of local squid proxy. Only the cvmfs client is used as it now (since 2.4.1) provides better cache-handling internals.
* 2017 11 06: Upgrade of CVMFS to version 2.4.1. Buggy: mountpoints not properly set.



## cvmfs-squid
#### *v0*
* 2018 01 15: New Docker images for cvmfs-squid. It plays the role of central, cluster-wise, proxy for CVMFS. Used only when deploying SWAN in a Kubernetes cluster with multiple SWAN worker nodes.



## ldap
#### *v0.2*
* 2018 08 08: Remove eos-mgm-lock (was eos-storage-lock) when deploying with docker-compose.
* 2018 08 08: Additional wait time (5 seconds) before adding demo users to ldap.
* 2018 04 11: Update of `addusers.sh` script to support custom LDAP configuration.
* 2018 04 11: Variables to botstrap LDAP server with custom configuration (e.g., Organization, Domain, Admin password, Read-only user, ...)

#### *v0.1*
* 2018 03 12: Update of `addusers.sh` script according to the new custom schema.
* 2018 03 12: Custom schema to support SSO to Unix identity mapping. Documented in `ldap.d/README.md`.

#### *v0*
* 2018 01 11: Remove lock-file for EOS storage only if deployment context is 'compose'.
* 2018 01 11: Remove sleep of 5 seconds waiting for the LDAP server to be ready.
* 2017 10 26: Update of `LDAP.yaml` to store server configuration and user database on external persistent volumes (e.g., OpenStack Cinder volumes).





## October 2017 -- Major Update Changelog
----

### eos-storage:v0
1. Software version:
 * eos-client-4.1.30, CITRINE
 * xrootd-4.7.0
2. Compatible with Kubernetes and docker-compose eos-controller container required for deployment).
3. Instance name changed to eosdocker (was eosdemo).
4. Root path changed to /eos/docker/ (was /eos/demo).


### eos-controller:latest
1. Used only by docker-compose to deploy EOS.
2. `eos-controller.d/start_eos.sh` improved to grant compatibility with new eos-storage image.
3. New environment variables:
  * "EOS_STORAGE_IMAGE_NAME" to specify which version of eos-storage image to be used. Deafults to "gitlab-registry.cern.ch/cernbox/boxedhub/eos-storage:latest".


### cernbox:v0
1. New GitHub branch "cernbox-prod-9.1.6" used as default branch for production.
2. Redis 3.2.10 installed as requirement for caching.
3. Solidification of known bugs and fixes included as part of building process (see end of Dockerfile).
4. New environment variables: 
  * "DEPLOYMENT_TYPE" to deploy according to docker-compose or Kubernetes requirements
  * "CERNBOXGATEWAY_HOSTNAME" to configure 'trusted_domains' and 'overwritehost' in `/root/cernbox.config.template` (was "BOX_HOSTNAME")
  * "EOS_MGM_ALIAS" to configure 'eosinstances.eos.mgm' (was hard-coded)
  * "LDAP_ENDPOINT" to configure 'cbox.ldap.user.hostname' (was hard-coded)


### cernboxgateway:v0
1. Preparation to have more flexibility in the deployment.
2. New environment variables:
  * "EOS_MGM_ALIAS" and "EOS_MGM_ALIAS_PORT" to specify eos-mgm endpoint 
  * "SWAN_BACKEND" and "SWAN_BACKEND_PORT" to specify jupyterhub endpoint
  * "OWNCLOUD_BACKEND" and "OWNCLOUD_BACKEND_PORT" to specify cernbox--owncloud backend endpoint
  * "LDAP_BACKEND1" and "LDAP_BACKEND2" to specify ldap server


### jupyterhub:v0
1. Updated JupyterHub version (0.7.2).
2. Updated dockerspawner (92a7ca676997dc77b51730ff7626d8fcd31860da).
3. Updated Jupyter version (0.5) in user containers.
4. Updated LDAP Authenticator (f3b2db14bfb591df09e05f8922f6041cc9c1b3bd).
5. New single-user session image "cernphsft/systemuser:v2.10" (was v2.9).
6. Different configuration files for deployment in docker-compose, Kuberetes, and kubernetes with KubeSpawner (still work-in-progress).
7. New mount point `eos/docker` (was `/eos/demo`) in `jupyterhub_config.docker.py`.
8. New environment variables:
  * "DEPLOYMENT_TYPE" to deploy according to docker-compose or Kubernetes requirements
  * "CONTAINER_IMAGE" to specify user container image without modifying `jupyterhub_config.docker.py` and avoid rebuilding the image (was hard-coded)
  * "LDAP_ENDPOINT" to specify the FQDN of the ldap server to connect to


### eos-fuse:v0
1. Upgrade of the base image (now cern/cc7-base:20170920).
2. Clean-up of tricks to start the eos daemon.
3. New repos to install eos-fuse and xrootd (same as eos-storage citrine) .
4. New environment variables:
  * "DEPLOYMENT_TYPE" to deploy according to docker-compose or Kubernetes requirements
  * "EOS_MGM_ENDPOINT" to specify the FQDN of the mgm to connect to
  * "LDAP_ENDPOINT" to specify the FQDN of the ldap server to connect to


### cmvfs:v0
1. New environment variables:
  * "DEPLOYMENT_TYPE" to deploy according to docker-compose or Kubernetes requirements


### ldap:v0
1. Now called ldap instead of openldap.
2. Hostname and Subdomain are explicited in the docker-compose.yaml file.
3. Ldap server becomes a parameter for all the services needing access to it. This implies new environment variable for ldap-ldapadd, cernbox, cernboxgateway, eos-storage (only the mgm requires access to LDAP), eos-fuse, jupyterhub. In the case of eos-controller, 'ldap.demonet' is hard-coded in the `start_eos.sh` script.





## October 2017 -- Major Update Notes
----
** Updates of Docker images for compatibility with Kubernetes**

### Changes in Docker image build // push // pull workflow
1. GitLab Registry supports Docker image versioning
2. Schema is 'gitlab-registry.cern.ch/cernbox/boxedhub/optional-image-name:tag'. E.g., 'gitlab-registry.cern.ch/cernbox/boxedhub/jupyterhub:v0'
3. All images should be built and pushed to GitLab independently on the others
4. As a consequence, the script 'PushImages.sh' is deprecated
5. Docker images being compatible with docker-compose and Kubernetes have tag 'v0'
6. Further software update, improvements, and modification will trigger a change in the tag
7. Docker image 'eos-controller' is the only one for which versioning is not applied: Only the tag 'latest' is accepted
8. docker-compose.yaml file references directly to new Docker images on GitLab. Pre-fetching has been maintained to speed-up uthe process and is configured so to pull new images from GitLab



### Changes in each service 

#### EOS-SERVER
1. eos-storage.d folder is now considered old and deprecated
2. eos-storage.{aquamarine, citrine}.d (and related Dockerfiles) take over eos-storage.d
3. EOS Aquamarine suffers of an issue for which attributes are not inherited by subfolders
4. EOS Aquamarine is no longer supported and maintained in its Docker-based version
5. EOS Citrine v4.x is the softare version maintained and distributed via Docker images


#### EOS-CONTROLLER
1. EOS Controller (eos-controller.d) is still required to deploy EOS-SERVER when using docker-compose
2. Docker image for EOS Controller has tag "gitlab-registry.cern.ch/cernbox/boxedhub/eos-controller"
3. No versioning is applied to EOS Controller image
4. Many parameters (e.g., component names, hostnames, volume mount points) are now parametrized to assure compatibility with the deployment in Kubernetes
5. The image name to use for deploying EOS can be passed as environment variable to eos-controller. Default value is 'gitlab-registry.cern.ch/cernbox/boxedhub/eos-storage:latest'
  

#### CERNBOX
1. New default branch for checkout is 'cernbox-prod-9.1.6'
2. Redis now used for caching

*TODO*

1. Use HTTPS instead of plain HTTP
2. Use alternate HTTP ports in case it must be co-located in the same Kubernetes Pod of cernboxgateway
3. Decide whether to log-in with CERN SSO -- eduGain or not


#### CERNBOXGATEWAY
*TODO*
Need to adapt to the flexibility of CERNBOX and JUPYTERHUB with KubeSpawner


#### JUPYTERHUB
1. New software version for JupyterHub, DockerSpawner, and LDAP Authenticator
2. New single-user session image 'cernphsft/systemuser:v2.10' (was v2.9)

*TODO*
 
1. Decide whether to log-in with CERN SSO -- eduGain or not
2. Implement CERNKubeSpawner for Kubernetes deloyments
3. As a consequence of CERNKubeSpawner, re-shape the deployment of eos-fuse and cvmfs daemons so to have them running as DaemonSets on all the nodes where user sessions can land


#### EOS-FUSE
1. Now uses the same repos of EOS Citrine
2. Clean-up of tricks and work-arounds to start the daemon inside the container
3. Extra capabilities, volumes, and share PID space with the host are still required. See more in eos-fuse.Dockerfile


#### CVMFS
*TODO*

1. Update to new version of CVMFS software: 2.4.x It implements local cache. As a consequence, squid proxy will no longer be required


#### LDAP
1. Renamed to ldap (was openldap) in docker-compose context

