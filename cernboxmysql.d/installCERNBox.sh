#!/bin/bash

###
# This file is used to install at run-time a vanilla CERNBox to initialize a MySQL database.
###

# Install software required by CERNBox
echo ""


# Clone CERNBox from Git


# Wait for the MySQL server to be ready
echo ""
echo "Wating for the MySQL server to be ready..." 
while ! mysqladmin ping --host="mysql.initmysqlnet" --user="$MYSQL_USER" --password="$MYSQL_PASSWORD"; do
  sleep 5
done

# Install CERNBox to initialize MySQL (MariaDB) running in the MySQL contianer
echo ""
echo "Installing CERNBox using MariaDB as backend...."
source /opt/rh/rh-php71/enable && \
  cd /var/www/html/cernbox && \
  rm -rf config/config.php && \
  ./occ maintenance:install --admin-user "_local" --admin-pass "_local" --database "mysql" --database-host "mysql.initmysqlnet" --database-user $MYSQL_USER --database-pass $MYSQL_PASSWORD --database-name $MYSQL_DATABASE --database-table-prefix "oc_" 

#import webng schema
echo "Updating DB schema for webng...."
mysql --host="mysql.initmysqlnet" --user="$MYSQL_USER" --password="$MYSQL_PASSWORD" --database="$MYSQL_DATABASE" < /hostPath/webng.sql

# Install CERNBox applications and enable them
echo ""
echo "Installing CERNBox applications..."
cd /var/www/html/cernbox/apps && \
  git clone -b reva https://github.com/cernbox/cernbox-theme && \
	git clone -b reva https://github.com/cernbox/files_texteditor && \
	git clone -b reva https://github.com/cernbox/files_pdfviewer && \
	git clone -b reva https://github.com/cernbox/cernboxauthtoken && \
	git clone -b reva https://github.com/cernbox/files_eostrashbin && \
	git clone -b reva https://github.com/cernbox/files_eosversions && \
	git clone -b reva https://github.com/cernbox/eosinfo && \
	git clone -b reva https://github.com/cernbox/gallery && \
	git clone -b reva https://github.com/cernbox/swanviewer && \
	git clone https://github.com/cernbox/smashbox && \
	git clone https://github.com/diocas/oauth2
	# git clone -b reva https://github.com/cernbox/files_projectspaces

# Disable default ones
source /opt/rh/rh-php71/enable && cd /var/www/html/cernbox && \
			./occ app:disable files_trashbin  && \
			./occ app:disable files_versions  && \
			./occ app:disable comments  && \
			./occ app:disable systemtags  && \
			./occ app:disable updatenotification && \
			./occ app:disable federation && \
			./occ app:disable provisioning_api

# Install reva apps
source /opt/rh/rh-php71/enable && cd /var/www/html/cernbox && \
			./occ app:enable cernbox-theme && \
			./occ app:enable files_pdfviewer && \
			./occ app:enable cernboxauthtoken && \
			./occ app:enable files_eostrashbin && \
			./occ app:enable files_eosversions && \
			./occ app:enable eosinfo && \
			./occ app:enable gallery && \
			./occ app:enable swanviewer && \
			./occ app:enable files_sharing && \
			./occ app:enable files_texteditor && \
			./occ app:enable oauth2


# Once the database is populated, remove the lock file and exit
rm -rf /hostPath/cernbox.lock

echo ""
echo "I am done!"
exit $?

