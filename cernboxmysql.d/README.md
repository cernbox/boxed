## MySQL backend for CERNBox
These instructions describe how to build the Docker image for MySQL (MariaDB) server to be used as database backend for CERNBox.
The procedure is completely automated, but manual steps for verification are still required.


### Context 
CERNBox can use either a SQLite or a MySQL database backend. 
  * SQLite is the default solution, with the database being colocated in the same container of the CERNBox service.
  * MySQL is the preferred solution production deployments. If MySQL is the choice, another container running the database server must be provided and executed.


### The problem
CERNBox populates the database with custom tables during the installation step, which takes place when building the CERNBox Docker image.
In order to support diverse deployment scenarios, the choice of the database backend is instead deferred to the run-time phase.
Being SQLite the default choice, the SQLite database is initialized when installing CERNBox but no action is taken with the MySQL databse.


### The solution
The Docker image for the MySQL server must already have on-board a pre-initialized database with the tables required by CERNBox.
To achieve this, scripts are provided to bootstrap the installation of CERNBox against a MySQL database backend. 
Files created on the MySQL side will be then made available for the build process of the cernboxmysql Docker image.


### How to initialize the MySQL database
Simply run the `initMySQL.sh` script.

The script invokes docker-compose, which starts a container with MySQL server (mariadb:5.5) and an empty container running CERN CentOS 7 (cern/cc7-base:20170922).
On the latter, a valilla CERNBox installation\* will take place and populate the MySQL database with the required tables.
At the end of the process, the populated database files will be compressed into `MySQL.tar.gz` and stored on the host machine. 
Right after, the container with CERNBox and MySQL will be torn down.
The entire setup should take about three minutes with a good Internet connection.

*\*Note: To pre-enable applications in CERNBox (e.g., text editor, PDF viewer, ...), these must be installed at enabled after the vanilla CERNBox installation.*

### What to do next
Once the `MySQL.tar.gz` file is created, the cernboxmysql Docker image can be built following the usual procedure for Docker.
Please, read the instruction on how to build, tag, and push the Docker image in `cernboxmysql.Dockerfile` in the parent folder.
The content of `MySQL.tar.gz` file will be copied in `/var/lib/mysql` on the Docker image providing the MySQL server for CERNBox.
MySQL will then be ready to accept requests from CERNBox out-of-the-box, without requiring any further configuration.


### Requirements
  * Internet connection
  * Docker
  * Docker-compose
  * tar

