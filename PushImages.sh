#!/bin/bash
# Push all the images in IMAGE_LIST to a Docker registry, e.g., gitlab-registry.cern.ch

# Note: This script pushes all the images in list to the same registry leveraging the 'tag' for setting the image name
#	This should be modified in the future so to have, instead, one repository per image

IMAGE_LIST="cernbox cernboxgateway eos-controller eos-storage openldap swan_cvmfs swan_eos-fuse swan_jupyterhub"

REMOTE_REPO="gitlab-registry.cern.ch/cernbox/boxedhub"

# Log in one for all
docker login gitlab-registry.cern.ch
echo ""

# Push the images
for image in ${IMAGE_LIST[*]};
do
	echo Pushing: $REMOTE_REPO:$image
	docker tag $image $REMOTE_REPO:$image	# Tag the image with the name the repo likes
	docker push $REMOTE_REPO:$image 	# Push
	docker rmi $REMOTE_REPO:$image		# Untag it
done
