#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

echo "---${THIS_CONTAINER}---"

case $DEPLOYMENT_TYPE in
  "kubernetes")
    # Print PodInfo
    echo ""
    echo "%%%--- PodInfo ---%%%"
    echo "Pod namespace: ${PODINFO_NAMESPACE}"
    echo "Pod name: ${PODINFO_NAME}"
    echo "Pod IP: ${PODINFO_IP}"
    echo "Node name (of the host where the pod is running): ${PODINFO_NODE_NAME}" 
    echo "Node IP (of the host where the pod is running): ${PODINFO_NODE_IP}"

    echo "Deploying with configuration for Kubernetes..."

    # Start crond for logrotation in supervisor
    echo "Enabling crond for logrotation..."
    mv /etc/supervisord.d/crond.noload /etc/supervisord.d/crond.ini

    if [ `echo $EOS_GATEWAY_SELF_REGISTRATION | tr '[:upper:]' '[:lower:]'` = "true" ]; then
      echo "Registering gateway with $EOS_GATEWAY_AUTH authentication..."
      bash /root/configure_gateway.sh add $EOS_GATEWAY_AUTH
    fi

    : '''
    echo "Checking to have a clean environment (mountpoints and folders)..."
    mounted_folders=`mount -l | grep "${EOS_INSTANCE_NAME} on ${EOS_FOLDER} " | cut -d ' ' -f 3 | tr '\n' ' '`
    if [[ -z $mounted_folders ]];
    then
      echo "Nothing to cleanup."
    else
      echo "Cleaning up: $mounted_folders"
      for i in $mounted_folders
      do
        umount $i
        rmdir $i	# It might fail if user containers are running
      done
    fi
    '''
    ;;

  ###
  "compose")
    echo "Deploying with configuration for Docker Compose..."
    # Check the lock before starting the daemon
    # Note: The lock is controlled by eos-controller. 
    #       It is needed to have the MGM up and running before starting the fuse daemon
    while [[ -f ${HOST_FOLDER}/eos-fuse-lock ]]
    do
      echo "Waiting for the lock to be removed"
      sleep 10
    done
    ;;

  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac


# Configure nscd and nslcd for LDAP access
echo "Starting LDAP services..."
sed -i "s|%%%LDAP_URI%%%|${LDAP_URI}|" /etc/nslcd.conf
sed -i "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /etc/nslcd.conf
sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/nslcd.conf
sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/nslcd.conf


# Start EOS daemon(s)
echo "Starting eosd..."
sed -i "s|%%%EOS_MGM_ALIAS%%%|${EOS_MGM_ALIAS}|" /etc/sysconfig/eos.docker
if [ "$EOS_MOUNT_PATH" ]; then
  sed -i "s|%%%EOS_MOUNT_PATH%%%|${EOS_MOUNT_PATH}|" /etc/sysconfig/eos.docker  
else
  sed -i "s|%%%EOS_MOUNT_PATH%%%|/eos/docker/|" /etc/sysconfig/eos.docker  
fi
service eosd start
service eosd status


# Start the processes and give control to supervisord
echo "Starting services..."
/usr/bin/supervisord -c /etc/supervisord.conf

