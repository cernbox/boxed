### DOCKER FILE FOR EOS AQUAMARINE -- EOS 0.3.x Version ###

### PLEASE SPECIFY EOS AND XROOTD VERSIONS ###
# E.g., 
# export XRD_VERSION="3.3.6"
# export EOS_VERSION="0.3.258"
# docker build -t eos-storage -f eos-storage.aquamarine.Dockerfile --build-arg EOS_VERSION="-${EOS_VERSION}" --build-arg XRD_VERSION="-${XRD_VERSION}" .
###


FROM cern/slc6-base:20170920

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Copy the repos to install eos and xrootd ----- #
ADD ./eos-storage.aquamarine.d/repos/*.repo /etc/yum.repos.d/

# ----- Install debugging tools ----- #
RUN yum -y install \
    gnutls \
    strace \
    gdb


# ----- Install XRootD ----- #
ARG XRD_VERSION
RUN yum -y --disablerepo=epel install \
    xrootd$XRD_VERSION \
    xrootd-client$XRD_VERSION \
    xrootd-client-devel$XRD_VERSION \
    xrootd-client-libs$XRD_VERSION \
    xrootd-devel$XRD_VERSION \
    xrootd-libs$XRD_VERSION \
    xrootd-server-devel$XRD_VERSION \
    xrootd-server-libs$XRD_VERSION


# ----- Install EOS ----- #
ARG EOS_VERSION
RUN yum -y --disablerepo=slc6-os --disablerepo=slc6-updates install \
    libmicrohttpd \
    libmicrohttpd-devel
RUN yum -y install \
    eos-client$EOS_VERSION \
    eos-server$EOS_VERSION \
    eos-testkeytab$EOS_VERSION


# ----- Install sssd to access user account information ----- #
# Note: This will be used by the MGM only
RUN yum -y install \
    nscd \
    nss-pam-ldapd \
    openldap-clients
ADD ./ldappam.d /etc
RUN chmod 600 /etc/nslcd.conf


# ----- Copy the configuration files for EOS components ----- #
# Note: Configuration files have to be modified in network/host/domain names so to be 
#		consistent with the configuration of other containers (e.g., SWAN, CERNBox)
ADD ./eos-storage.aquamarine.d/config/eos.sysconfig /etc/sysconfig/eos
ADD ./eos-storage.aquamarine.d/config/xrd.cf.* /etc/
ADD ./eos-storage.aquamarine.d/config/eos_*.sh /root/eos_setup/
ADD ./eos-storage.aquamarine.d/configure_eos_namespace.sh /root/

# ----- Copy the bootstrap script ----- #
ADD ./eos-storage.aquamarine.d/start.sh /root/start.sh
CMD ["/bin/bash", "/root/start.sh"]

