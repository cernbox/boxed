# Dockerfile to create the container for SmashBox for CERN-box demo
# ----- Use CERN cc7 as base image for EOS|FUSE ----- #
FROM cern/cc7-base:20170920

MAINTAINER Igor Tkachenko <igor.tkachenko@cern.ch>

RUN yum -y install yum-plugin-ovl # See http://unix.stackexchange.com/questions/348941/rpmdb-checksum-is-invalid-trying-to-install-gcc-in-a-centos-7-2-docker-image
RUN yum -y update
COPY smashbox.d/owncloud.repo /etc/yum.repos.d/
RUN yum -y install git owncloud-client python python2-pip

ENV smashboxPath '/usr/local/smashbox'
ENV smashboxConfig 'smashbox.conf'
ENV smashboxUser 'user0'
ENV smashboxUserPassword 'test0'
ENV configScript 'doConfig.sh'

RUN mkdir -p "`dirname ${smashboxPath}`"
RUN git clone https://github.com/cernbox/smashbox.git "${smashboxPath}" 
RUN pip install -r "${smashboxPath}/requirements.txt"

COPY "smashbox.d/${smashboxConfig}.template" "${smashboxPath}/etc/${smashboxConfig}.template"
RUN /usr/bin/sed \
  -e "s|%%%DIR%%%|${smashboxPath}|" \
  -e "s|%%%USER%%%|${smashboxUser}|" \
  -e "s|%%%PASSWORD%%%|${smashboxUserPassword}|" \
  -e 's|%%%OC_SERVER%%%|cernboxgateway.demonet|' \
  -e 's|%%%OC_ROOT%%%|cernbox/desktop|' \
  -e 's|%%%OC_CMD%%%|owncloudcmd --trust|' \
  -e 's|%%%OC_ADMIN_USER%%%||' \
  -e 's|%%%OC_ADMIN_PASSWORD%%%||' \
  "${smashboxPath}/etc/${smashboxConfig}.template" > "${smashboxPath}/etc/${smashboxConfig}"

# ----- Run the setup script in the container ----- #
COPY smashbox.d/sleepLoop.sh /sleepLoop.sh
RUN chmod +x /sleepLoop.sh
CMD ["bash", "/sleepLoop.sh"]
