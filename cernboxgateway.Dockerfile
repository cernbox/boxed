### DOCKER FILE FOR cernboxgateway IMAGE ###

###
# export RELEASE_VERSION=":v0"
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/cernboxgateway${RELEASE_VERSION} -f cernboxgateway.Dockerfile .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/cernboxgateway${RELEASE_VERSION}
###


FROM cern/slc6-base:20180622

MAINTAINER Jakub Moscicki <jakub.moscicki@cern.ch>


# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8


# ----- Install the required packages ----- #
RUN yum -y install \
        perl \
        git


# ----- Install and configure the gateway ----- #
ADD ./cernboxgateway.d/install/common.sh /root/install/common.sh
ADD ./cernboxgateway.d/install/install_cernbox_proxy_server /root/install/install_cernbox_proxy_server
ADD ./cernboxgateway.d/install/install_cernbox_proxy_server.config.d /root/install/install_cernbox_proxy_server.config.d
ADD ./cernboxgateway.d/install/private.example /root/install/private.example
ADD ./cernboxgateway.d/install/nginx.example /root/nginx

RUN bash /root/install/install_cernbox_proxy_server


# ----- Copy SSL certificates to serve the gateway over HTTPS ----- #
# NOTE: These certificates might be overridden at run time by the ones available in uboxed/certs/boxed.{key,crt}
ADD ./secrets/boxed.crt /etc/boxed/certs/boxed.crt
ADD ./secrets/boxed.key /etc/boxed/certs/boxed.key
ADD ./secrets/dhparams.pem /etc/grid-security/dhparams.pem

# ----- Install uwsgi to serve the python script for "Open in SWAN" button ----- #
RUN yum -y install \
        uwsgi \
        uwsgi-plugin-python \
        uwsgi-devel.x86_64 \
        uwsgi-plugin-python2.x86_64
RUN mkdir -p /run/uwsgi 
ADD ./cernboxgateway.d/uwsgi.conf /etc/uwsgi.ini
ADD ./cernboxgateway.d/cgi-bin/go /var/www/cgi-bin/go
RUN chmod +x /var/www/cgi-bin/go


# ----- Create a landing page ----- #
ADD ./cernboxgateway.d/welcome_page/ /var/www/html/
RUN cp /var/www/html/welcome.html_CERN /var/www/html/welcome/index.html


# ----- Install the EOS client, copy the gateway keytab, and set the correct 
#       permissions to register the cernboxgateway container as HTTPS gateway ----- #
RUN yum -y install eos-client
ADD ./eos-storage.citrine.d/utils/configure_gateway.sh /root/configure_gateway.sh
ADD ./eos-storage.citrine.d/keytabs/eos-gateway.keytab /etc/eos-gateway.keytab
RUN chown daemon:daemon /etc/eos-gateway.keytab
RUN chmod 600 /etc/eos-gateway.keytab


# ----- Install supervisord ----- #
RUN yum -y install supervisor
# Note: This is the only contianer using a different configuration file for supervisord
#       as the supervisord version coming with SLC6 is older that the one for CC7.
ADD ./cernboxgateway.d/supervisor/supervisord.conf /etc/supervisord.conf
ADD ./supervisord.d/kill_supervisor.ini /etc/supervisord.d/kill_supervisor.ini
ADD ./supervisord.d/kill_supervisor.py /etc/supervisord.d/kill_supervisor.py


# ----- Run crond under supervisor and copy configuration files for log rotation ----- #
RUN yum -y install cronie
ADD ./supervisord.d/crond.ini /etc/supervisord.d/crond.noload
ADD ./logrotate.d/logrotate /etc/cron.hourly/logrotate
RUN chmod +x /etc/cron.hourly/logrotate


# ----- Install logrotate and copy configuration files ----- #
RUN yum -y install logrotate
RUN mv /etc/logrotate.conf /etc/logrotate.defaults
ADD ./logrotate.d/logrotate.conf /etc/logrotate.conf

# Copy logrotate jobs for JupyterHub
RUN rm -f /etc/logrotate.d/nginx
ADD ./logrotate.d/logrotate.jobs.d/nginx /etc/logrotate.d/nginx


# ----- Start the services ----- #
ADD ./cernboxgateway.d/start_nginx/ /root/start_nginx/
ADD ./cernboxgateway.d/start.sh /root/start.sh
ADD ./cernboxgateway.d/stop.sh /root/stop.sh

CMD ["/bin/bash", "/root/start.sh"]

