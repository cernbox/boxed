#! /bin/bash

source /etc/sysconfig/eos

# /eos/config not writable by daemon (?), fix needed for aquamarine
mkdir -p /var/eos/config/
chown -R daemon:root /var/eos

# Precedence to libjemalloc
test -e /usr/lib64/libjemalloc.so.1 && export LD_PRELOAD=/usr/lib64/libjemalloc.so.1

echo "Starting mgm..."
/usr/bin/xrootd -n mgm -c /etc/xrd.cf.mgm -m -l /var/log/eos/xrdlog.mgm -b -Rdaemon

eos -b vid enable sss
eos -b vid enable unix
eos -b vid set membership daemon +sudo

echo "Done with mgm."

