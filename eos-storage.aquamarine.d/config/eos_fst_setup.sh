#! /bin/bash

if [ -z ${EOS_FST_NUMBER} ]; then
    echo "ERROR: fst number not set."
    echo "Cannot continue."
    exit 1
fi


source /etc/sysconfig/eos
export EOS_MGM_URL=root://${EOS_MGM_ALIAS}//

echo "Starting fst ${EOS_FST_NUMBER}..."
/usr/bin/xrootd -n fst -c /etc/xrd.cf.fst -l /var/log/eos/xrdlog.fst -b -Rdaemon

echo "Creating data directory for fst ${EOS_FST_NUMBER}..."
UUID=fst${EOS_FST_NUMBER}
DATADIR=/home/data/eos${EOS_FST_NUMBER}
mkdir -p ${DATADIR}
echo "${UUID}" > ${DATADIR}/.eosfsuuid
echo "${EOS_FST_NUMBER}" > ${DATADIR}/.eosfsid
chown -R daemon:daemon ${DATADIR}

echo "Adding file system ${EOS_FST_NUMBER}..."
FSTHOSTNAME=$(hostname -f)
eos -b fs add -m ${EOS_FST_NUMBER} ${UUID} ${FSTHOSTNAME}:1095 ${DATADIR} default rw
eos -r 0 0 -b node set ${FSTHOSTNAME}:1095 on

echo "Done with fst ${EOS_FST_NUMBER}."

