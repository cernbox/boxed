#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

echo "---${THIS_CONTAINER}---"

echo "Removing IPv6 entries from /etc/hosts..."
cp /etc/hosts /etc/hosts.backup && grep -v "ip6-" /etc/hosts.backup > /etc/hosts

case $DEPLOYMENT_TYPE in
  "kubernetes")
    # Print PodInfo
    echo ""
    echo "%%%--- PodInfo ---%%%"
    echo "Pod namespace: ${PODINFO_NAMESPACE}"
    echo "Pod name: ${PODINFO_NAME}"
    echo "Pod IP: ${PODINFO_IP}"
    echo "Node name (of the host where the pod is running): ${PODINFO_NODE_NAME}" 
    echo "Node IP (of the host where the pod is running): ${PODINFO_NODE_IP}"

    echo "Deploying with configuration for Kubernetes..."

    # Start EOS service
    echo ""
    echo "Deploying eos node with role: $EOS_ROLE"
    ###bash /root/eos_setup/eos_${EOS_ROLE}_setup.sh
    ;;
  ###
  "compose")
    echo "Deploying with configuration for Docker Compose..."
    ;;

  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac

if [[ ${EOS_ROLE} == "mgm" ]]; then
  echo "Starting nscd and nslcd services..."
  sed -i "s/%%%LDAP_ENDPOINT%%%/${LDAP_ENDPOINT}/" /etc/nslcd.conf
  nscd
  nslcd
fi


# Done
echo ""
echo "Ready!"
sleep infinity

