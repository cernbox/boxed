#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

if [ "$#" -ne 1 ]; then
        echo "ERROR: Illegal number of parameters."
        echo "Syntax: configure_quotaadmin.sh <action>"
        echo "Example: configure_quotaadmin.sh set"
        echo "Example: configure_quotaadmin.sh rm"
        exit 1
fi

QUOTAADMIN_ACTION=`echo $1 | tr '[:upper:]' '[:lower:]'`

# Set the keytab to get sudo permissions on eos
export EOS_MGM_URL=root://$EOS_MGM_ALIAS
export XrdSecPROTOCOL=sss
export XrdSecsssKT=$EOS_GATEWAY_KEYTAB_FILE
if [ -z "$EOS_GATEWAY_KEYTAB_FILE" ]; then
  export XrdSecsssKT=/etc/eos-gateway.keytab
  echo "WARNING: EOS keytab file not set."
  echo "WARNING: Defaulting to $XrdSecsssKT"
fi


# Check if `eos whoami` return a valid IPv4 address
function validate_ip {
  IP=$1
  stat=1

  if [[ $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    OIFS=$IFS
    IFS='.'
    IP_OCTETS=($IP)
    [[ ${IP_OCTETS==[0]} -le 255 && ${IP_OCTETS==[1]} -le 255 && ${IP_OCTETS==[2]} -le 255 && ${IP_OCTETS==[3]} -le 255 ]]
      stat=$?
    IFS=$OIFS
  fi
  return $stat
}


# Helper function
function register_quotaadmin {
  FAILED=false
  case $1 in
    "set")
      echo "Configuring quota administrator..."
      eos -b -r 0 0 vid set membership adm +sudo || FAILED=true
      eos -b -r 0 0 vid set map -tident "*@$2" vuid:`id -u adm` vgid:`id -g adm` || FAILED=true
      ;;

    ###
    "remove")
      echo "Removing quota administrator..."
      eos -b -r 0 0 vid rm tident:\"*@$2\":uid || FAILED=true
      eos -b -r 0 0 vid rm tident:\"*@$2\":gid || FAILED=true
      ;;

    ###
    *)
      echo "ERROR: Required action is unknown."
      echo "Supported actions are: set remove"
      exit -1
  esac

  if $FAILED ; then
    echo "ERROR: Unable to configure as quota administrator"
  fi
}


# Register current node as EOS gateway
WHOAMI=`eos -b whoami | rev | cut -d '=' -f 1 | rev`
register_quotaadmin $QUOTAADMIN_ACTION $WHOAMI

###
# NOTE: This is a workaround
#
# The ability to configure the same node as gateway in both IPv4 and IPv6 address
# formats can be of help in case of buggy network stacks.
# The EOS MGM process is started with the IPv4 stack only but in some cases the
# address of a client is resolve to an IPv6 adapted address, i.e., with a prepended
# "::ffff:" to the IPv4 address of the client.
# By adding both the native IPv4 the IPv6-adapted client addresses to the EOS 
# gateway list, the client is then trusted as a gateway.
if validate_ip $WHOAMI ; then
  register_quotaadmin $QUOTAADMIN_ACTION `echo "[::ffff:"$WHOAMI"]"`
fi

