#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

echo "Unlocking attachment of filesystems from FSTs."
rm ${HOST_FOLDER}/eos-fst-lock

echo "Unlocking eos-fuse client."
rm ${HOST_FOLDER}/eos-fuse-lock

echo "Unlocking cernbox."
rm ${HOST_FOLDER}/cernbox-lock

echo "Unlocking cernboxgateway."
rm ${HOST_FOLDER}/cernboxgateway-lock

echo "I'm done. Exiting..."
