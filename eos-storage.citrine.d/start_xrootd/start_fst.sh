#! /bin/bash

echo "Starting FST daemon..."

# Command to run
CMD="/usr/bin/xrootd -n fst -c /etc/xrd.cf.fst -l /var/log/eos/xrdlog.fst -Rdaemon -I v4"

# Kill old processes (required by restart on fail within the container)
kill `ps auxf | grep "$CMD" | grep "^daemon" | tr -s ' ' | cut -d ' ' -f 2 | tr '\n' ' '` >/dev/null 2>&1

# Start the process
source /etc/sysconfig/eos
test -e /usr/lib64/libjemalloc.so.1 && export LD_PRELOAD=/usr/lib64/libjemalloc.so.1
eval $CMD

