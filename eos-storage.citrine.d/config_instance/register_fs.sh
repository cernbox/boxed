#! /bin/bash

cmd_timeout() {
  timeout=$1
  shift
  cmd=$@
  t=0
  exit=0
  $cmd &
  pid=$!

  while [[ $t -lt $timeout ]] && [ $exit -eq 0 ] ; do
    sleep 1
    (( t = t + 1 ))
    proc=$(ps -ef | awk '{print $2}' | grep "$pid" )
    if [[ -z "$proc" ]] ; then
      (( exit = 1 ))
    fi
  done

  if [[ $exit -ne 1 ]] ; then
    kill -9 $pid
	echo "TIMEOUT - process killed"
  fi
}


##### ----- #####
export XrdSecPROTOCOL=sss
export XrdSecsssKT=/etc/eos.keytab
export EOS_MGM_URL=root://${EOS_MGM_ALIAS}

SLEEP="30s"
echo "Waiting for xrootd process to be ready..."
echo "Sleeping for $SLEEP..."
sleep $SLEEP


if [ "$DEPLOYMENT_TYPE" == "compose" ]; then
  echo "Waiting for the MGM to be configured before attaching filesystems..."
    while [[ -f ${HOST_FOLDER}/eos-fst-lock ]]
    do
      echo 'Waiting for the lock to be removed'
      sleep 10
    done
fi


# Configuring file system and node the eos-ops way
# See: https://gitlab.cern.ch/dss/eos-ops-scripts/blob/master/eosscripts/eos-register-all-filesystem-inside-eos-instance.sh
#
# NOTE: This is currently limited to ONE fs for FST container
#

# Check the mount point to be owned by daemon:daemon
if [[ `stat -c "%U:%G" $FST_MOUNTPOINT` != "daemon:daemon" ]]
then
  echo "Changing ownership of $FST_MOUNTPOINT to daemon:daemon"
  echo "Previous ownership was "`stat -c "%U:%G" $FST_MOUNTPOINT`
  chown --preserve-root daemon:daemon $FST_MOUNTPOINT
fi
echo "$FST_MOUNTPOINT is owned by "`stat -c "%U:%G" $FST_MOUNTPOINT`

# Register in EOS_INSTANCE_NAME only the mount point not registered yet
if [[ -e "$FST_MOUNTPOINT/.eosfsid" ]]
then
  # Already registered
  echo "FSID already assigned to $FST_MOUNTPOINT"
  ls -la $FST_MOUNTPOINT/.eos*
  cat $FST_MOUNTPOINT/.eosfsid
  echo
  cat $FST_MOUNTPOINT/.eosfsuuid
  echo
else
  # Not registered
  status=`cmd_timeout 180 /usr/sbin/eosfstregister $FST_MOUNTPOINT $FST_SCHEDULING_GROUP:1` # EOS-2142
  sleep 5
  echo "$status"
fi

# Enable the node in EOS when done
status=`cmd_timeout 180 /usr/sbin/eosadmin -b -r 0 0 node set $FST_FQDN on`
echo "$status"
status=`cmd_timeout 180 /usr/sbin/eosadmin -b -r 0 0 node ls -l $FST_FQDN`
echo "$status"

echo "Done."

