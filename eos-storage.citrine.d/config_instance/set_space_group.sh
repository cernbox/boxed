#! /bin/bash

echo "Configuring default space and group..."
eos -b space define default
eos -b space set default on
eos -b space quota default off

eos -b group set default.0 on

echo "Done."
