#! /bin/bash

echo "Configuring trusted gateways..."

case $DEPLOYMENT_TYPE in
  ###
  "kubernetes")
    # NOTE: It is better to register the CERNBox web container manually with its Kubernetes service
    #       name as, dependingly on deployment context, it can be resolved to cernbox.cernbox.boxed....
    #       instead of using the IP address of the container.
    echo "Registering CERNBox Web container via Kubernetes service..."
    eos vid add gateway cernbox.cernbox.boxed.svc.cluster.local unix
    eos vid set membership adm +sudo
    eos vid set map -tident "*@cernbox.cernbox.boxed.svc.cluster.local" vuid:`id adm -u` vgid:`id adm -g`

    # NOTE: CERNBox, CERNBox-gateway, and fuse-daemons are able to self-register themselves via SSS.
    #       This is due to the fact that they will not have stable network addresses and hostnames.
    #       The registration of the gateway will be done using the IP address only.
    echo "Gateways w/o a Kubernetes service will self-register..."
    echo "Done."
  ;;

  ###
  "compose")
    admin_uid=`id -u dummy_admin`
    admin_gid=`id -g dummy_admin`
    eos vid set membership ${admin_uid} +sudo

    eos vid add gateway cernbox unix
    eos vid add gateway cernbox.demonet unix
    eos vid set map -tident "*@cernbox" vuid:${admin_uid} vgid:${admin_gid}
    eos vid set map -tident "*@cernbox.demonet" vuid:${admin_uid} vgid:${admin_gid}

    eos vid add gateway eos-fuse unix
    eos vid add gateway eos-fuse.demonet unix

    eos vid add gateway cernboxgateway https
    eos vid add gateway cernboxgateway.demonet https
  ;;

  ###
  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac

