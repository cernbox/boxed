### DOCKER FILE FOR eos-controller IMAGE ###

###
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/eos-controller -f eos-controller.Dockerfile .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/eos-controller
###


FROM cern/cc7-base:20170920

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# ----- Install the required packages to spawn EOS core containers ----- #
RUN yum -y install wget
RUN wget -q https://get.docker.com -O /tmp/getdocker.sh && \
	bash /tmp/getdocker.sh && \
	rm /tmp/getdocker.sh

# ----- Copy the script to orchestrate the deployment of EOS core containers ----- #
COPY eos-controller.d/start_eos.sh /root

# ----- Deploy EOS core containers ----- #
CMD ["/bin/bash", "/root/start_eos.sh"]
