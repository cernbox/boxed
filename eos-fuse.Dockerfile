### DOCKER FILE FOR eos-fuse IMAGE ###

### PLEASE SPECIFY EOS AND XROOTD VERSIONS ###
# E.g., 
# export XRD_VERSION="4.8.4"
# export EOS_VERSION="4.4.27"
# export RELEASE_VERSION=":v0"
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/eos-fuse${RELEASE_VERSION} -f eos-fuse.Dockerfile --build-arg EOS_VERSION="-${EOS_VERSION}" --build-arg XRD_VERSION="-${XRD_VERSION}" .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/eos-fuse${RELEASE_VERSION}
###

# NOTE: The container needs SYS_ADMIN capabilities (--cap-add SYS_ADMIN), access to /dev/fuse on the host machine (--device /dev/fuse), and have the PID space shared with the host machine (--pid host).
# WARNING: Given the shared PID space, running this container on a host where eos daemons are already
#			in execution may interfere with their normal operation.
# 
#	-->	To run the container WITHOUT EOS access from the outside of the container: 
#			1. docker build -t eos -f eos.Dockerfile .
#			2. docker run --name eos_mount --cap-add SYS_ADMIN --device /dev/fuse --pid host -t eos
#
#	-->	To run the container WITH EOS access from the outside of the container: 
#			1. mkdir -p /deploy_docker/eos_mount
#				==> MAKE SURE THE DIRECTORY IS EMPTY! <==
#			2. docker build -t eos -f eos.Dockerfile .
#			3. docker run --name eos_mount --cap-add SYS_ADMIN --device /dev/fuse --pid host --volume /deploy_docker/eos_mount:/eos:shared -t eos
#		-->	If you then need to access EOS from another docker container, 
#			run the latter as, e.g., :
#			docker run -it --volume /deploy_docker/eos_mount:/eos ubuntu bash
#		--> To remove the /deploy_docker/eos_mount subfolders when the EOS container is gone run
#				fusermount -u /deploy_docker/eos_mount/<folder_to_be_removed>, or
#				sudo umount -l /deploy_docker/eos_mount/<folder_to_be_removed>
#			and the proceed with the typical rmdir <folder_to_be_removed>
#

### BUILD and RUN for testing ###
# docker build -t eos-fuse -f eos-fuse.Dockerfile . && \
# docker run --rm --device /dev/fuse --volume /sys/fs/cgroup --pid host --privileged --security-opt apparmor:unconfined --cap-add SYS_ADMIN -it eos-fuse bash
###


FROM cern/cc7-base:20180516

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8


# ----- Remove yum autoupdate ----- #
RUN yum -y remove \
    yum-autoupdate


# ----- Install tools for LDAP access ----- #
RUN yum -y install \
	nscd \
	nss-pam-ldapd \
	openldap-clients
ADD ./ldappam.d /etc
RUN chmod 600 /etc/nslcd.conf
ADD ./ldappam.d/nslcd_foreground.sh /usr/sbin/nslcd_foreground.sh
RUN chmod +x /usr/sbin/nslcd_foreground.sh


# ----- Copy the repos to install eos and xrootd ----- #
ADD ./eos-storage.citrine.d/repos/*.repo /etc/yum.repos.d/


# ----- Install the xrootd client ----- #
ARG XRD_VERSION
RUN yum -y --disablerepo=cern install \
	xrootd$XRD_VERSION \
	xrootd-client$XRD_VERSION \
	xrootd-client-libs$XRD_VERSION \
	xrootd-libs$XRD_VERSION


# ----- Install the eos-fuse client ----- #
ARG EOS_VERSION
RUN yum -y --disablerepo=updates --disablerepo=cern --disablerepo=xrootd-stable install \
	eos-client$EOS_VERSION \
	eos-fuse$EOS_VERSION \
	initscripts
RUN mkdir -p /run/lock/subsys


# ----- Copy the configuration files for eos ----- #
ADD ./eos-fuse.d/eos.conf.d/eos.baseline /etc/sysconfig/eos
ADD ./eos-fuse.d/eos.conf.d/eos.docker /etc/sysconfig/eos.docker
#ADD ./eos-fuse.d/eos.conf.d/CERN/eos.baseline /etc/sysconfig/eos
#ADD ./eos-fuse.d/eos.conf.d/CERN/eos.opendata /etc/sysconfig/eos.opendata
#ADD ./eos-fuse.d/eos.conf.d/CERN/eos.user /etc/sysconfig/eos.user


# ----- Copy the gateway keytab and set the correct permissions ----- #
ADD ./eos-storage.citrine.d/utils/configure_gateway.sh /root/configure_gateway.sh
ADD ./eos-storage.citrine.d/keytabs/eos-gateway.keytab /etc/eos-gateway.keytab
RUN chown daemon:daemon /etc/eos-gateway.keytab
RUN chmod 600 /etc/eos-gateway.keytab


# ----- Install supervisord and base configuration file ----- #
RUN yum -y install supervisor
ADD ./supervisord.d/supervisord.conf /etc/supervisord.conf
ADD ./supervisord.d/kill_supervisor.ini /etc/supervisord.d
ADD ./supervisord.d/kill_supervisor.py /etc/supervisord.d

# Copy supervisor ini files
ADD ./supervisord.d/nscd.ini /etc/supervisord.d
ADD ./supervisord.d/nslcd.ini /etc/supervisord.d


# ----- Run crond and copy configuration files for hourly/daily log rotation ----- #
ADD ./supervisord.d/crond.ini /etc/supervisord.d/crond.noload
ADD ./logrotate.d/logrotate /etc/cron.hourly/logrotate
RUN chmod +x /etc/cron.hourly/logrotate


# ----- Install logrotate and copy the configuration files ----- #
RUN yum -y install logrotate
RUN mv /etc/logrotate.conf /etc/logrotate.defaults
ADD ./logrotate.d/logrotate.conf /etc/logrotate.conf
# Note: No need to define a logrotation job. This is provided by the eos installation


# ----- Run the setup script in the container ----- #
ADD ./eos-fuse.d/start.sh /root/start.sh 
ADD ./eos-fuse.d/stop.sh /root/stop.sh
CMD ["/bin/bash", "/root/start.sh"]

