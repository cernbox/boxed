# Docker file for EOS components based on Aquamarine
# Version 0.2
#
# Credits go to Elvin Sindrilaru and József Makai, CERN 2017

### PLEASE BUILD FROM ONE LEVEL UP IN THE DIRECTORY TREE ###
## e.g., docker build -t eos-storage -f eos-storage.d/citrine.Dockerfile --build-arg EOS_VERSION=${EOS_VERSION} --build-arg XRD_VERSION=${XRD_VERSION} .



#FROM cern/cc7-base:20170113
FROM centos:7
# TODO: Using centos for the time being (MGM is not able to boot with cc7)
#	This should be fixed

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


RUN yum -y install yum-plugin-ovl # See http://unix.stackexchange.com/questions/348941/rpmdb-checksum-is-invalid-trying-to-install-gcc-in-a-centos-7-2-docker-image


# ==> EOS CITRINE -- EOS 4 Version
COPY eos-storage.d/citrine.d/eos_citrine.repo /etc/yum.repos.d/eos.repo
COPY eos-storage.d/citrine.d/epel_citrine.repo /etc/yum.repos.d/epel.repo
RUN yum-config-manager --enable eos eos-depend epel epel-debuginfo epel-source
RUN yum -y install \
	gnutls \
	strace \
	gdb
RUN yum -y --disablerepo=base --disablerepo=updates install \
    libmicrohttpd \
    protobuf3


# ----- Install XRootD ----- #
ARG XRD_VERSION
RUN yum -y --nogpg install \
    xrootd$XRD_VERSION \
    xrootd-client$XRD_VERSION \
    xrootd-client-libs$XRD_VERSION \
    xrootd-libs$XRD_VERSION \
    xrootd-server-devel$XRD_VERSION \
    xrootd-server-libs$XRD_VERSION


# ----- Install EOS ----- #
ARG EOS_VERSION
RUN yum -y --nogpg install \
    eos-server$EOS_VERSION \
    eos-client$EOS_VERSION \
    eos-testkeytab$EOS_VERSION \
    quarkdb


# ----- Install sssd to access user account information ----- #
# Note: This will be used by the MGM only
RUN yum -y --nogpg install \
    nscd \
    nss-pam-ldapd \
    openldap-clients


# ----- Copy the configuration files for EOS components ----- #
# Note: Configuration files have to be modified in network/host/domain names so to be 
#		consistent with the configuration of other containers (e.g., SWAN, CERNBox)
ADD eos-storage.d/citrine.d/config/eos.sysconfig /etc/sysconfig/eos
ADD eos-storage.d/citrine.d/config/xrd.cf.* /etc/
ADD eos-storage.d/citrine.d/config/eos_*.sh /
ADD eos-storage.d/configure_eos_namespace.sh /


# ----- Copy the configuration files for user account information ----- #
# Note: This will be used by the MGM only
ADD ldappam.d /etc


CMD ["/bin/bash"]
